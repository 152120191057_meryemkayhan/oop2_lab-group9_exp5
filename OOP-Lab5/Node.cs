﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Lab5
{
    public struct Node
    {
        public Node(int ID,string Name, string Price, string Description,/* string Filename,*/ string Path)
        {
            this.ID = ID;
            name = Name;
            price = Price;
            description = Description;
            path = Path;
        }
        public int ID;
        public string name;
        public string price;
        public string description;
        public string path;
    }
}
