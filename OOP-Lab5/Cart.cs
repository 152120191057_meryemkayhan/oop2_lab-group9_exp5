﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;

namespace OOP_Lab5
{
    public class Cart
    {
        String connectionstring = "Data Source=SQL5063.site4now.net;Initial Catalog=db_a75584_ooplab;User Id=db_a75584_ooplab_admin;Password=7eQ2icRhm_au";
        SqlConnection connection;
        SqlDataAdapter adapter;
        SqlCommand command;
        private List<CartNode> myCart = new List<CartNode>();
        public Cart() { }

        public void addToCart(CartNode item) // seçilen itemı ekler
        {
            string name = item.name;
            int index = myCart.FindIndex(x => x.name == (name));

            if (index < 0)
                myCart.Add(item);
            else
                updateAmountOfProduct(index, item.amount + myCart[index].amount);
        }
        public void deleteFromCart(string name) // yalnızca seçilen itemı siler
        {
            int index = myCart.FindIndex(x => x.name == (name));
            myCart.RemoveAt(index);
        }

        public void clearMyCart(int id) // tüm sepeti tek tuşla boşaltır
        {
            connection = new SqlConnection(connectionstring);
            connection.Open();

            string selectsentence = "Select * from ShoppingCart";
            adapter = new SqlDataAdapter(selectsentence, connection);
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);
            try
            {
                string SelectDelete = "delete from ShoppingCart where UserID=" + id.ToString();
                command = new SqlCommand(SelectDelete, connection);
                command.ExecuteNonQuery();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            connection.Close();
            myCart.Clear();
        }
        public void updateAmountOfProduct(int index, uint newAmount) // ürünün sepetteki miktarını günceller
        {
            
            CartNode temp = myCart[index];
            uint price = temp.price / temp.amount;
            temp.amount = newAmount;
            temp.price = newAmount * price;
            myCart[index] = temp;
        }
        public void updateAmountOfProduct(string name, uint newAmount) // ürünün sepetteki miktarını günceller
        {
            int index = myCart.FindIndex(x => x.name == (name));
            CartNode temp = myCart[index];
            uint price = temp.price / temp.amount;
            temp.amount = newAmount;
            temp.price = newAmount * price;
            myCart[index] = temp;
        }
        public string[] GetListElement(int index)
        {
            string[] liste = { myCart[index].name, myCart[index].price.ToString(), myCart[index].amount.ToString(), myCart[index].path };

            return liste;
        }
        public int GetListSize() { return myCart.Count(); }
        public int GetIndex(string name)
        {
            int index = myCart.FindIndex(x => x.name == (name));
            return index;
        }
    }
}
