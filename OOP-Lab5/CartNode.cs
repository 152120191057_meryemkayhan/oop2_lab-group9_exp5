﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Lab5
{
    public struct CartNode
    {
        public CartNode(string name, uint price, uint amount, string path )
        {
            this.name = name;
            this.price = price;
            this.amount = amount;
            this.path = path;
        }

        // bunu private yapıp get fonk yazmalıyım.
        // müşteri ekranında kimsenin name değiştirme yetkisine sahip olmasına lüzum yok. 
        public string name;
        // price, güncellenen amout miktarına göre değişmeli her cartNode u için. 
        public uint price;
        public uint amount;
        public string path;
    }
}
