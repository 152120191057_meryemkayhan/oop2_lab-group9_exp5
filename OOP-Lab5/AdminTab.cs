﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OOP_Lab5;
using System.IO;
using System.Drawing.Drawing2D;
using System.Data.SqlClient;
using System.Linq.Expressions;

namespace OOP_Lab5
{
    public partial class AdminTab : Form
    {
        List<Node> DataList = new List<Node>();
        static string constring = "Data Source = SQL5063.site4now.net; Initial Catalog = db_a75584_ooplab; User Id = db_a75584_ooplab_admin; Password=7eQ2icRhm_au";
        SqlConnection connection = new SqlConnection(constring);
        DataTable table;
        SqlCommand command;
        SqlDataAdapter adapter;
        Store store = new Store();
        Person loggedPerson = new Person();
        ProductDescription proDesc = new ProductDescription();
        CartForm cartForm = new CartForm();
        Image image;
       
        private static AdminTab inst;
        public static AdminTab GetForm
        {
            get
            {
                if (inst == null || inst.IsDisposed)
                    inst = new AdminTab();
                return inst;
            }
        }
        public AdminTab()
        {
            InitializeComponent();
            connection.Open();
            string selectsentence = "Select * from Product";
            adapter = new SqlDataAdapter(selectsentence, connection);
            table = new DataTable();
            adapter.Fill(table);

            for (int i = 0; i < table.Rows.Count; i++)
            {
                Node product = new Node(int.Parse(table.Rows[i]["ProductID"].ToString()), table.Rows[i]["Name"].ToString(), table.Rows[i]["Price"].ToString(), table.Rows[i]["Description"].ToString(), Path.Combine(Environment.CurrentDirectory,table.Rows[i]["Path"].ToString()));
                DataList.Add(product);
            }
            connection.Close();
        }
        public void setPerson(Person autPerson)
        {
            loggedPerson = autPerson;
            loggedPerson.Id = autPerson.Id;
            loggedPerson.cart = autPerson.cart;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            listView1.View = View.Details;
            listView1.Columns.Add("Product", 75);
            listView1.Columns.Add("Price", 75);
            listView1.Columns.Add("Description", 75);
            listView1.Columns.Add("Path", 150);
            string[] information;

            for (int i = 0; i < size(); i++) { 
                information = GetListElement(i);
                ListViewItem item = new ListViewItem(information);
                listView1.Items.Add(item);
            }    
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            string fileName = "";
            string path = "";
            string directory = "";
            bool picturestate=false;
            bool flag = true;
            
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                directory = openFileDialog1.FileName;
                fileName = directory.Substring(directory.LastIndexOf(@"\") + 1);
                path = Path.Combine(Environment.CurrentDirectory, @"pictures\", fileName);
                picturestate = true;
            }
            string PathData = Path.Combine(@"pictures\", fileName);
            
            for (int i = 0; i < DataList.Count; i++)
            {
                if (DataList[i].name == Name)
                    flag = false;
            }
            if (flag == true)
            {
                try
                {
                    if (txtName.Text != "" && txtPrice.Text != "" && txtDesc.Text != "" && PathData != "")
                    {
                        Node node = new Node(listView1.Items.Count + 1, txtName.Text, txtPrice.Text, txtDesc.Text, PathData);
                        connection.Open();
                        string input = "Insert Into Product(ProductID,Name,Price,Description,Path) values (@ProductID,@Name, @Price, @Description, @Path)";
                        command = new SqlCommand(input, connection);
                        command.Parameters.Add("@ProductID", listView1.Items.Count + 1);
                        command.Parameters.Add("@Name", txtName.Text);
                        command.Parameters.Add("@Price", int.Parse(txtPrice.Text));
                        command.Parameters.Add("@Description", txtDesc.Text);
                        command.Parameters.Add("@Path", PathData);
                        command.ExecuteNonQuery();
                        connection.Close();
                        DataList.Add(node);
                    }
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.ToString());
                }
            }
            if (picturestate == true)
            {
                image = Image.FromFile(openFileDialog1.FileName);
                if (openFileDialog1.FileName != path)
                    image.Save(path);

                string[] information = { txtName.Text, txtPrice.Text, txtDesc.Text, Path.Combine(Environment.CurrentDirectory,PathData)};
                ListViewItem newAdd = new ListViewItem(information);

                listView1.Items.Add(newAdd);
                lblError.Text = "";
            }
            else if (picturestate == false)
                lblError.Text = "There is no selected image to add";
            else
                lblError.Text = "Invalid informations.";     
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            string fileName = "";
            string path = "";
            string directory = "";
            string pathData = "";
            bool picturestate = false;
            lblError.Text = "";
            bool updated = false;
            if (listView1.SelectedItems.Count > 0)
            {
                if (chcboxPicture.Checked == true)
                {
                    if (openFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        directory = openFileDialog1.FileName;
                        fileName = directory.Substring(directory.LastIndexOf(@"\") + 1);
                        path = Path.Combine(Environment.CurrentDirectory, @"pictures\", fileName);
                        pathData = Path.Combine(@"pictures\", fileName);
                        picturestate = true;
                    }                 
                    else
                    {
                        directory = openFileDialog1.FileName;
                        picturestate = false;
                    }
                }
                try {            
                    if (chcboxPicture.Checked == true)
                    {
                        image = Image.FromFile(openFileDialog1.FileName);
                        if (openFileDialog1.FileName != path)
                            image.Save(path);
                        else
                            lblError.Text = "Picture path has not changed.";
                        
                    }
                    int idx = DataList.FindIndex(x => x.name == (listView1.SelectedItems[0].SubItems[0].Text));
                    var temp = DataList[idx];                   
                    if (temp.name != null)
                    {
                        if (txtName.Text != "" && txtName.Text != temp.name)
                        {
                            string sqlEkle = "Update Product set Name = @Name where Name='" + temp.name + "'";
                            connection.Open();
                            command = new SqlCommand(sqlEkle, connection);
                            command.Parameters.AddWithValue("@Name", txtName.Text);
                            command.ExecuteNonQuery();
                            connection.Close();
                            temp.name = txtName.Text;
                            updated = true;
                        }
                        if (txtPrice.Text != "" && txtPrice.Text != temp.price)
                        {
                            string sqlEkle = "UPDATE Product Set Price=@Price where Name='" + temp.name + "'";
                            connection.Open();
                            command = new SqlCommand(sqlEkle, connection);
                            command.Parameters.AddWithValue("@Price", int.Parse(txtPrice.Text));
                            command.ExecuteNonQuery();
                            connection.Close();
                            temp.price = txtPrice.Text;
                            updated = true;
                        }
                        if (txtDesc.Text != "" && txtDesc.Text != temp.description)
                        {
                            string sqlEkle = "Update Product set Description = @Description where Name='" + temp.name + "'";
                            connection.Open();
                            command = new SqlCommand(sqlEkle, connection);
                            command.Parameters.AddWithValue("@Description", txtDesc.Text);
                            command.ExecuteNonQuery();
                            connection.Close();
                            temp.description = txtDesc.Text;
                            updated = true;
                        }
                    }
                    if (pathData != "" && chcboxPicture.Checked == true)
                    {
                        string sqlEkle = "Update Product set Path = @Path where Name='" + temp.name + "'";
                        connection.Open();
                        command = new SqlCommand(sqlEkle, connection);
                        command.Parameters.AddWithValue("@Path", pathData);
                        command.ExecuteNonQuery();
                        connection.Close();
                        temp.path = pathData;
                        updated = true;
                    }
                    DataList[idx] = temp;                                  
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                if (chcboxPicture.Checked == true && picturestate == false)
                    lblError.Text = "There is no selected image to update";
                else if (txtName.Text == listView1.SelectedItems[0].SubItems[0].Text && txtPrice.Text == listView1.SelectedItems[0].SubItems[1].Text && txtDesc.Text == listView1.SelectedItems[0].SubItems[2].Text)
                    lblError.Text = "Any value is not changed.";
                if ((txtName.Text == "" || txtPrice.Text == "" || txtDesc.Text == "") && chcboxPicture.Checked == true && picturestate == true)
                    lblError.Text = "Picture updated but informations didn't update \n because of invalid input.";
                pictureBox1.ImageLocation = listView1.FocusedItem.SubItems[3].Text.ToString();
                
                if (updated == true) { 
                    if (txtName.Text != "")
                        listView1.SelectedItems[0].SubItems[0].Text = txtName.Text;
                    if (txtPrice.Text != "")
                        listView1.SelectedItems[0].SubItems[1].Text = txtPrice.Text;
                    if (txtDesc.Text != "")
                        listView1.SelectedItems[0].SubItems[2].Text = txtDesc.Text;
                    if (chcboxPicture.Checked == true)
                        listView1.SelectedItems[0].SubItems[3].Text = Path.Combine(Environment.CurrentDirectory,pathData);
                }
            }          
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            
            if (DeleteControl(txtName.Text))
            {
                listView1.Items.RemoveAt(listView1.SelectedIndices[0]);
                txtName.Text = "";
                txtDesc.Text = "";
                txtPrice.Text = "";
            }
            else
                lblError.Text = "There is not a/n element which has/n entered name.";
        }
        public bool DeleteControl(string Name)
        {
            Node node = DataList.Find(x => x.name.Equals(Name));
            if (node.name != "")
            {
                try
                {   connection.Open();
                    
                    string SelectDelete = "delete from Product where Name = '" + node.name + "'";
                    command = new SqlCommand(SelectDelete, connection);
                    command.ExecuteNonQuery();
                    
                    SelectDelete = "Delete from ShoppingCart where ProductID = '" + node.ID + "'";
                    command = new SqlCommand(SelectDelete, connection);
                    command.ExecuteNonQuery();
                    
                    DataList.Remove(DataList.Find(x => x.name.Equals(Name)));
                    connection.Close();
                    
                    return true;
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message);
                    return false;
                }
            }
            else
                return false;
        }
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            pictureBox1.ImageLocation = listView1.FocusedItem.SubItems[3].Text.ToString();

            txtName.Text = listView1.FocusedItem.SubItems[0].Text.ToString();
            txtPrice.Text = listView1.FocusedItem.SubItems[1].Text.ToString();
            txtDesc.Text = listView1.FocusedItem.SubItems[2].Text.ToString();
        }
        private void btnStore_Click(object sender, EventArgs e)
        {
            this.Hide();
            store.setPerson(loggedPerson);
            proDesc.setPerson(loggedPerson);
            cartForm.setPerson(loggedPerson);
            store.Show();
        }
        public string[] GetListElement(int index)
        {
            string[] liste = { DataList[index].name, DataList[index].price, DataList[index].description, DataList[index].path };

            return liste;
        }
        public string[] GetListElement(string name)
        {
            int index = DataList.FindIndex(x => x.name == (name));
            string[] liste = { DataList[index].ID.ToString(),DataList[index].name, DataList[index].price, DataList[index].description };
            return liste;
        }
        public int size()
        {
            return DataList.Count;
        }
        public string[] GetImageProperty(string name)
        {
            int index = DataList.FindIndex(x => x.name == (name));
            string[] temp = { DataList[index].path };
            return temp;
        }
        public string GetImageProperty(int index)
        {
            string temp = DataList[index].path;
            return temp;
        }

        private void AdminTab_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
                Authentication.GetForm.Show();
        }
    }
}
