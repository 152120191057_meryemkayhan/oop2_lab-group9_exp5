﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OOP_Lab5;
using System.Data.SqlClient;

namespace OOP_Lab5
{
    public partial class Authentication : Form
    {
        AdminTab adminTab = new AdminTab();
        Person loggedPerson = new Person();
        List<Person> People = new List<Person>();
        ProductDescription proDesc = new ProductDescription();
        CartForm cartForm = new CartForm();

        SqlConnection sqlConnection;
        SqlCommand sqlCommand;
        SqlDataAdapter sqlDataAdapter;

        private static Authentication inst;
        public static Authentication GetForm
        {
            get
            {
                if (inst == null || inst.IsDisposed)
                    inst = new Authentication();
                return inst;
            }
        }
        public Authentication()
        {
            InitializeComponent();
            txtBox_password_signUp.PasswordChar = '*';
            txtBox_password.PasswordChar = '*';
            textBox_confirmPassword.PasswordChar = '*';
        }
        public bool isLoginClicked { get; set; }
        private void Authentication_Load(object sender, EventArgs e)
        {
            sqlConnection = new SqlConnection("Data Source=SQL5063.site4now.net; Initial Catalog=db_a75584_ooplab; User ID=db_a75584_ooplab_admin; Password=7eQ2icRhm_au");
            sqlConnection.Open();
            sqlDataAdapter = new SqlDataAdapter("select * from Users", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                Person customer = new Person(int.Parse(dataTable.Rows[i]["userID"].ToString()),
                                                dataTable.Rows[i]["username"].ToString(),
                                                dataTable.Rows[i]["email"].ToString(),
                                                dataTable.Rows[i]["password"].ToString(),
                                                dataTable.Rows[i]["address"].ToString(),
                                                dataTable.Rows[i]["phoneNumber"].ToString(),
                                                dataTable.Rows[i]["profilePicture"].ToString(),
                                                Convert.ToBoolean(dataTable.Rows[i]["isAdmin"]));
                People.Add(customer);
            }
            sqlConnection.Close();
        }

        public static bool detect_Permission(List<Person> customers, string TxtInput_username, string TxtInput_password)
        {
            string hashedPsw = CryptoLib.Encryptor.MD5Hash(TxtInput_password);
            for (int i = 0; i < customers.Count; i++)
            {
                if (TxtInput_username == customers[i].Username)
                {
                    if (TxtInput_password == customers[i].Password)
                    {
                        return true;
                    }
                    /// şifrsini yanlış giren dikkatsiz bir kullanıcı. kapıdan göndermek için 0 return ediyoruz.
                    return false;
                }
            }
            /// böyle bir kullanıcı yok ki şifresi doğru olsun
            return false;
        }
        // formdan alınan şifrenin people listi içinde tutulan person kullanıcılarından herhangi biriyle eşleşip eşleşmediğine
        // eşleşiyor ise adminle mi yoksa adminin, sayelerinde para kazanacağı müşterilerle mi eşleştiğine baktığımız fonksiyon aşağıdadır.
        private int detect_PermissionLevel(List<Person> People, string TxtInput_username, string TxtInput_password)
        {
            string hashedPsw = CryptoLib.Encryptor.MD5Hash(TxtInput_password);

            for (int i = 0; i < People.Count; i++)
            {
                if (TxtInput_username == People[i].Username)
                {
                    if (hashedPsw == People[i].Password)
                    {
                        if (People[i].IsAdmin == false) return 1; // access permission level = 1 means the user is çinko karbon vatandaş
                        else return 2; // permission of access level is 2 means the user is admin.
                    }
                }
            }
            return 0; // şifrsini yanlış giren dikkatsiz bir kullanıcı. kapıdan göndermek için 0 return ediyoruz.
        }

        // giriş yapmaya çalışan insancığın kayıtlı kullanıcı olup olmadığına baktığımız fonksiyonumuz:
        private bool detect_usernameTaken(List<Person> People, string TxtInput_username)
        {
            for (int i = 0; i < People.Count; i++)
            {
                if (TxtInput_username == People[i].Username) return true;
            }
            return false;
        }
        // registered email cheeeeck
        private bool detect_emailTaken(List<Person> People, string TxtInput_email)
        {
            for (int i = 0; i < People.Count; i++)
            {
                if (TxtInput_email == People[i].Email) return true;
            }
            return false;
        }
        private void btn_login_Click(object sender, EventArgs e)
        {
            // şifre ve kullanıcı adı text box larının boş olması durumu 
            if (String.IsNullOrEmpty(txtBox_username.Text))
            {
                if (String.IsNullOrEmpty(txtBox_password.Text))
                {
                    lbl_errorMsg_login.Text = "You must enter your username and password.";
                    lbl_errorMsg_login.ForeColor = Color.Red;
                    return;
                }
                else
                {
                    lbl_errorMsg_login.Text = "You must enter a username.";
                    lbl_errorMsg_login.ForeColor = Color.Red;
                    return;
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtBox_password.Text))
                {
                    lbl_errorMsg_login.Text = "You must enter a password.";
                    lbl_errorMsg_login.ForeColor = Color.Red;
                    return;
                }
            }
            bool hasAccount = detect_usernameTaken(People, txtBox_username.Text);
            int permissionLevel = detect_PermissionLevel(People, txtBox_username.Text, txtBox_password.Text);

            if (hasAccount == true) // hesap var
            {
                Store store = new Store();
                if (permissionLevel == 0) // yanlış şifre
                {
                    lbl_errorMsg_login.Text = "Wrong Password! Try Again. ";
                    lbl_errorMsg_login.ForeColor = Color.Red;
                }
                else if (permissionLevel == 1) // çinko karbon müşteri hesabı
                {
                    // bi kaç saniye bekleme yapmadığımız için başarılı giriş mesajları göremeyeceğimiz kadar hızlı akıcak zaten following 2 satır silinebilir ama neyse
                    lbl_errorMsg_login.Text = "Successful login!";
                    lbl_errorMsg_login.ForeColor = Color.Green;

                    // giriş yapmış kullanıcıyı belirleme
                    for (int i = 0; i < People.Count; i++)
                    {
                        if (txtBox_username.Text == People[i].Username) loggedPerson = People[i];
                    }
                    //Form açma işlemleri
                    this.Hide();
                    txtBox_username.Text = "";
                    txtBox_password.Text = "";
                    txtBox_password_signUp.Text = "";
                    textBox_confirmPassword.Text = "";
                    txtBox_username_signUp.Text = "";
                    lbl_errorMsg_signup.Text = "";
                    lbl_errorMsg_login.Text = "";
                    isLoginClicked = true;
                    try
                    {
                        store.Show();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        Application.Exit();
                    }

                    store.setPerson(loggedPerson);
                    proDesc.setPerson(loggedPerson);
                    cartForm.setPerson(loggedPerson);
                }
                else if (permissionLevel == 2) // admin
                {
                    // bi kaç saniye bekleme yapmadığımız için başarılı giriş mesajları göremeyeceğimiz kadar hızlı akıcak zaten following 2 satır silinebilir ama neyse
                    lbl_errorMsg_login.Text = "Successful login!";
                    lbl_errorMsg_login.ForeColor = Color.Green;
                    // giriş yapmış kullanıcıyı belirleme
                    for (int i = 0; i < People.Count; i++)
                    {
                        if (txtBox_username.Text == People[i].Username) loggedPerson = People[i];
                    }
                    //Form açma işlemleri

                    this.Hide();
                    txtBox_username.Text = "";
                    txtBox_password.Text = "";
                    txtBox_password_signUp.Text = "";
                    textBox_confirmPassword.Text = "";
                    txtBox_username_signUp.Text = "";
                    lbl_errorMsg_signup.Text = "";
                    lbl_errorMsg_login.Text = "";
                    isLoginClicked = true;

                    adminTab.Show();
                    int no = 0;
                    no = Convert.ToInt32(loggedPerson.Id.ToString());
                    adminTab.setPerson(loggedPerson);
                    store.setPerson(loggedPerson);
                    proDesc.setPerson(loggedPerson);
                    cartForm.setPerson(loggedPerson);
                }
            }
            else // böyle bir hesap yok
            {
                lbl_errorMsg_login.Text = "No Such User Here! Please, Sign Up First. ";
                lbl_errorMsg_login.ForeColor = Color.Red;
            }
        }
        private void btn_signUp_Click(object sender, EventArgs e)
        {

            bool hasAccount = detect_emailTaken(People, textBox_email.Text);

            if (hasAccount == true)
            {
                lbl_errorMsg_signup.ForeColor = Color.Red;
                lbl_errorMsg_signup.Text = "This email is already registered.\nIf it is yours, you can sign in.";
                return;
            }
            bool invalidUsername = detect_usernameTaken(People, txtBox_username_signUp.Text);
            if (invalidUsername == true)
            {
                lbl_errorMsg_signup.ForeColor = Color.Red;
                lbl_errorMsg_signup.Text = "This username is already taken.\nChoose another one.";
                return;
            }

            // şifre ve kullanıcı adı text box larının boş olması durumu 
            if (String.IsNullOrEmpty(txtBox_password_signUp.Text) || String.IsNullOrEmpty(textBox_confirmPassword.Text))
            {
                if (String.IsNullOrEmpty(txtBox_username_signUp.Text) ||
                    String.IsNullOrEmpty(textBox_email.Text) ||
                     String.IsNullOrEmpty(textBox_address.Text) ||
                     String.IsNullOrEmpty(textBox_phoneNumber.Text))
                {
                    lbl_errorMsg_signup.Text = "These fields cannot be empty.";
                    lbl_errorMsg_signup.ForeColor = Color.Red;
                    return;
                }
                else
                {
                    lbl_errorMsg_signup.Text = "You must confirm your password.";
                    lbl_errorMsg_signup.ForeColor = Color.Red;
                    return;
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtBox_username_signUp.Text))
                {
                    lbl_errorMsg_signup.Text = "You must enter a username.";
                    lbl_errorMsg_signup.ForeColor = Color.Red;
                    return;
                }
            }
            if (invalidUsername == false) // kullanıcı adı uygunsa
            {
                if (txtBox_password_signUp.Text != textBox_confirmPassword.Text)
                {
                    lbl_errorMsg_signup.ForeColor = Color.Red;
                    lbl_errorMsg_signup.Text = "Make sure your passwords match.";
                }
                else // nihayet kayıt işlemi gerçekleştirilebilir 
                {
                    string hashedPsw = CryptoLib.Encryptor.MD5Hash(txtBox_password_signUp.Text);
                    lbl_errorMsg_signup.Text = "";
                    txtBox_password_signUp.ForeColor = Color.Black;
                    textBox_confirmPassword.ForeColor = Color.Black;
                    txtBox_username_signUp.ForeColor = Color.Black;
                    lbl_errorMsg_signup.ForeColor = Color.Green;
                    lbl_errorMsg_signup.Text = "Signed Up Succesfully, Now Please Log In.";
                   
                    // database e yeni kullanıcıyı aktarma
                    sqlConnection = new SqlConnection("Data Source=SQL5063.site4now.net; Initial Catalog=db_a75584_ooplab; User ID=db_a75584_ooplab_admin; Password=7eQ2icRhm_au");
                    sqlConnection.Open();
                    string sorgu = "Insert Into Users(username,email,password,address,phoneNumber,profilePicture,isAdmin) values(@username,@email,@password,@address,@phoneNumber,@profilePicture,@isAdmin)";
                    sqlCommand = new SqlCommand(sorgu, sqlConnection);

                    sqlCommand.Parameters.AddWithValue("@username", txtBox_username_signUp.Text);
                    sqlCommand.Parameters.AddWithValue("@email", textBox_email.Text);
                    sqlCommand.Parameters.AddWithValue("@password", CryptoLib.Encryptor.MD5Hash(txtBox_password_signUp.Text));
                    sqlCommand.Parameters.AddWithValue("@address", textBox_address.Text);
                    sqlCommand.Parameters.AddWithValue("@phoneNumber", textBox_phoneNumber.Text);
                    sqlCommand.Parameters.AddWithValue("@profilePicture", "pictures\\pp.jpg");
                    sqlCommand.Parameters.AddWithValue("@isAdmin", Convert.ToByte(false));
                    sqlCommand.ExecuteNonQuery();
                    sqlConnection.Close();
                    // sign up ettikten sonra programda çalışan databaseden userId yi çekip çıkarmam lazım.
                    // aslında hepsini tekrar almaya gerek yok. zaten form açılırken yeni kayıt dışındakilerin hepsi geliyor.

                    sqlConnection = new SqlConnection("Data Source=SQL5063.site4now.net; Initial Catalog=db_a75584_ooplab; User ID=db_a75584_ooplab_admin; Password=7eQ2icRhm_au");
                    sqlConnection.Open();
                    sqlDataAdapter = new SqlDataAdapter("select * from Users", sqlConnection);
                    DataTable dataTable = new DataTable();
                    sqlDataAdapter.Fill(dataTable);

                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        Person customer = new Person(int.Parse(dataTable.Rows[i]["userID"].ToString()),
                                                        dataTable.Rows[i]["username"].ToString(),
                                                        dataTable.Rows[i]["email"].ToString(),
                                                        dataTable.Rows[i]["password"].ToString(),
                                                        dataTable.Rows[i]["address"].ToString(),
                                                        dataTable.Rows[i]["phoneNumber"].ToString(),
                                                        dataTable.Rows[i]["profilePicture"].ToString(),
                                                        Convert.ToBoolean(dataTable.Rows[i]["isAdmin"]));
                        People.Add(customer);
                    }
                    sqlConnection.Close();
                }
            }
        }
        private void Authentication_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
        private void tab_login_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox_email.Text = "";
            txtBox_username_signUp.Text = "";
            textBox_address.Text = "";
            textBox_phoneNumber.Text = "";
            txtBox_password_signUp.Text = "";
            textBox_confirmPassword.Text = "";
            lbl_errorMsg_signup.Text = "";
            txtBox_username.Text = "";
            txtBox_password.Text = "";
        }
    }
}

