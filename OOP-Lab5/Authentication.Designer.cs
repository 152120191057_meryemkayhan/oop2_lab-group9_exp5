﻿
namespace OOP_Lab5
{
    partial class Authentication
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.UserName = new System.Windows.Forms.Label();
            this.Password = new System.Windows.Forms.Label();
            this.txtBox_username = new System.Windows.Forms.TextBox();
            this.txtBox_password = new System.Windows.Forms.TextBox();
            this.btn_login = new System.Windows.Forms.Button();
            this.tab_login = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lbl_errorMsg_login = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.textBox_phoneNumber = new System.Windows.Forms.TextBox();
            this.lbl_phoneNumber = new System.Windows.Forms.Label();
            this.textBox_address = new System.Windows.Forms.TextBox();
            this.lbl_address = new System.Windows.Forms.Label();
            this.textBox_email = new System.Windows.Forms.TextBox();
            this.lbl_email = new System.Windows.Forms.Label();
            this.lbl_errorMsg_signup = new System.Windows.Forms.Label();
            this.textBox_confirmPassword = new System.Windows.Forms.TextBox();
            this.lbl_confirmPassword = new System.Windows.Forms.Label();
            this.btn_signUp = new System.Windows.Forms.Button();
            this.txtBox_username_signUp = new System.Windows.Forms.TextBox();
            this.lbl_password_signUp = new System.Windows.Forms.Label();
            this.txtBox_password_signUp = new System.Windows.Forms.TextBox();
            this.lbl_Username_signUp = new System.Windows.Forms.Label();
            this.errorProvider_ConfirmPassw = new System.Windows.Forms.ErrorProvider(this.components);
            this.tab_login.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_ConfirmPassw)).BeginInit();
            this.SuspendLayout();
            // 
            // UserName
            // 
            this.UserName.AutoSize = true;
            this.UserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(168)))), ((int)(((byte)(156)))));
            this.UserName.Location = new System.Drawing.Point(110, 184);
            this.UserName.Name = "UserName";
            this.UserName.Size = new System.Drawing.Size(145, 32);
            this.UserName.TabIndex = 0;
            this.UserName.Text = "Username";
            // 
            // Password
            // 
            this.Password.AutoSize = true;
            this.Password.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Password.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(168)))), ((int)(((byte)(156)))));
            this.Password.Location = new System.Drawing.Point(117, 243);
            this.Password.Name = "Password";
            this.Password.Size = new System.Drawing.Size(139, 32);
            this.Password.TabIndex = 1;
            this.Password.Text = "Password";
            // 
            // txtBox_username
            // 
            this.txtBox_username.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_username.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(68)))), ((int)(((byte)(85)))));
            this.txtBox_username.Location = new System.Drawing.Point(266, 184);
            this.txtBox_username.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtBox_username.MinimumSize = new System.Drawing.Size(200, 34);
            this.txtBox_username.Name = "txtBox_username";
            this.txtBox_username.Size = new System.Drawing.Size(200, 34);
            this.txtBox_username.TabIndex = 5;
            // 
            // txtBox_password
            // 
            this.txtBox_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_password.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(68)))), ((int)(((byte)(85)))));
            this.txtBox_password.Location = new System.Drawing.Point(266, 243);
            this.txtBox_password.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtBox_password.MinimumSize = new System.Drawing.Size(200, 34);
            this.txtBox_password.Name = "txtBox_password";
            this.txtBox_password.Size = new System.Drawing.Size(200, 34);
            this.txtBox_password.TabIndex = 6;
            // 
            // btn_login
            // 
            this.btn_login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(168)))), ((int)(((byte)(156)))));
            this.btn_login.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_login.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_login.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_login.Location = new System.Drawing.Point(224, 320);
            this.btn_login.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_login.Name = "btn_login";
            this.btn_login.Size = new System.Drawing.Size(120, 50);
            this.btn_login.TabIndex = 7;
            this.btn_login.Text = "Log In";
            this.btn_login.UseVisualStyleBackColor = false;
            this.btn_login.Click += new System.EventHandler(this.btn_login_Click);
            // 
            // tab_login
            // 
            this.tab_login.Controls.Add(this.tabPage1);
            this.tab_login.Controls.Add(this.tabPage2);
            this.tab_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tab_login.Location = new System.Drawing.Point(40, 11);
            this.tab_login.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tab_login.Name = "tab_login";
            this.tab_login.SelectedIndex = 0;
            this.tab_login.Size = new System.Drawing.Size(591, 626);
            this.tab_login.TabIndex = 9;
            this.tab_login.SelectedIndexChanged += new System.EventHandler(this.tab_login_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPage1.Controls.Add(this.lbl_errorMsg_login);
            this.tabPage1.Controls.Add(this.txtBox_username);
            this.tabPage1.Controls.Add(this.UserName);
            this.tabPage1.Controls.Add(this.btn_login);
            this.tabPage1.Controls.Add(this.Password);
            this.tabPage1.Controls.Add(this.txtBox_password);
            this.tabPage1.Location = new System.Drawing.Point(4, 38);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(583, 584);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Log In";
            // 
            // lbl_errorMsg_login
            // 
            this.lbl_errorMsg_login.AutoSize = true;
            this.lbl_errorMsg_login.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(112)))), ((int)(((byte)(90)))));
            this.lbl_errorMsg_login.Location = new System.Drawing.Point(111, 378);
            this.lbl_errorMsg_login.Name = "lbl_errorMsg_login";
            this.lbl_errorMsg_login.Size = new System.Drawing.Size(0, 29);
            this.lbl_errorMsg_login.TabIndex = 8;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPage2.Controls.Add(this.textBox_phoneNumber);
            this.tabPage2.Controls.Add(this.lbl_phoneNumber);
            this.tabPage2.Controls.Add(this.textBox_address);
            this.tabPage2.Controls.Add(this.lbl_address);
            this.tabPage2.Controls.Add(this.textBox_email);
            this.tabPage2.Controls.Add(this.lbl_email);
            this.tabPage2.Controls.Add(this.lbl_errorMsg_signup);
            this.tabPage2.Controls.Add(this.textBox_confirmPassword);
            this.tabPage2.Controls.Add(this.lbl_confirmPassword);
            this.tabPage2.Controls.Add(this.btn_signUp);
            this.tabPage2.Controls.Add(this.txtBox_username_signUp);
            this.tabPage2.Controls.Add(this.lbl_password_signUp);
            this.tabPage2.Controls.Add(this.txtBox_password_signUp);
            this.tabPage2.Controls.Add(this.lbl_Username_signUp);
            this.tabPage2.Location = new System.Drawing.Point(4, 38);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Size = new System.Drawing.Size(583, 584);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Sign Up";
            // 
            // textBox_phoneNumber
            // 
            this.textBox_phoneNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_phoneNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(68)))), ((int)(((byte)(85)))));
            this.textBox_phoneNumber.Location = new System.Drawing.Point(309, 244);
            this.textBox_phoneNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox_phoneNumber.MinimumSize = new System.Drawing.Size(200, 34);
            this.textBox_phoneNumber.Name = "textBox_phoneNumber";
            this.textBox_phoneNumber.Size = new System.Drawing.Size(200, 34);
            this.textBox_phoneNumber.TabIndex = 8;
            // 
            // lbl_phoneNumber
            // 
            this.lbl_phoneNumber.AutoSize = true;
            this.lbl_phoneNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_phoneNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(168)))), ((int)(((byte)(156)))));
            this.lbl_phoneNumber.Location = new System.Drawing.Point(80, 244);
            this.lbl_phoneNumber.Name = "lbl_phoneNumber";
            this.lbl_phoneNumber.Size = new System.Drawing.Size(205, 32);
            this.lbl_phoneNumber.TabIndex = 7;
            this.lbl_phoneNumber.Text = "Phone Number";
            // 
            // textBox_address
            // 
            this.textBox_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_address.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(68)))), ((int)(((byte)(85)))));
            this.textBox_address.Location = new System.Drawing.Point(309, 173);
            this.textBox_address.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox_address.MinimumSize = new System.Drawing.Size(200, 34);
            this.textBox_address.Name = "textBox_address";
            this.textBox_address.Size = new System.Drawing.Size(200, 34);
            this.textBox_address.TabIndex = 6;
            // 
            // lbl_address
            // 
            this.lbl_address.AutoSize = true;
            this.lbl_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_address.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(168)))), ((int)(((byte)(156)))));
            this.lbl_address.Location = new System.Drawing.Point(166, 173);
            this.lbl_address.Name = "lbl_address";
            this.lbl_address.Size = new System.Drawing.Size(119, 32);
            this.lbl_address.TabIndex = 5;
            this.lbl_address.Text = "Address";
            // 
            // textBox_email
            // 
            this.textBox_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_email.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(68)))), ((int)(((byte)(85)))));
            this.textBox_email.Location = new System.Drawing.Point(309, 31);
            this.textBox_email.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox_email.MinimumSize = new System.Drawing.Size(200, 34);
            this.textBox_email.Name = "textBox_email";
            this.textBox_email.Size = new System.Drawing.Size(200, 34);
            this.textBox_email.TabIndex = 2;
            // 
            // lbl_email
            // 
            this.lbl_email.AutoSize = true;
            this.lbl_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_email.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(168)))), ((int)(((byte)(156)))));
            this.lbl_email.Location = new System.Drawing.Point(198, 33);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(87, 32);
            this.lbl_email.TabIndex = 1;
            this.lbl_email.Text = "Email";
            // 
            // lbl_errorMsg_signup
            // 
            this.lbl_errorMsg_signup.AutoSize = true;
            this.lbl_errorMsg_signup.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(168)))), ((int)(((byte)(156)))));
            this.lbl_errorMsg_signup.Location = new System.Drawing.Point(52, 512);
            this.lbl_errorMsg_signup.Name = "lbl_errorMsg_signup";
            this.lbl_errorMsg_signup.Size = new System.Drawing.Size(0, 29);
            this.lbl_errorMsg_signup.TabIndex = 20;
            // 
            // textBox_confirmPassword
            // 
            this.textBox_confirmPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_confirmPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(68)))), ((int)(((byte)(85)))));
            this.textBox_confirmPassword.Location = new System.Drawing.Point(309, 386);
            this.textBox_confirmPassword.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox_confirmPassword.MinimumSize = new System.Drawing.Size(200, 34);
            this.textBox_confirmPassword.Name = "textBox_confirmPassword";
            this.textBox_confirmPassword.Size = new System.Drawing.Size(200, 34);
            this.textBox_confirmPassword.TabIndex = 12;
            // 
            // lbl_confirmPassword
            // 
            this.lbl_confirmPassword.AutoSize = true;
            this.lbl_confirmPassword.BackColor = System.Drawing.Color.Transparent;
            this.lbl_confirmPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_confirmPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(168)))), ((int)(((byte)(156)))));
            this.lbl_confirmPassword.Location = new System.Drawing.Point(40, 386);
            this.lbl_confirmPassword.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_confirmPassword.Name = "lbl_confirmPassword";
            this.lbl_confirmPassword.Size = new System.Drawing.Size(245, 32);
            this.lbl_confirmPassword.TabIndex = 11;
            this.lbl_confirmPassword.Text = "Confirm Password";
            // 
            // btn_signUp
            // 
            this.btn_signUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(168)))), ((int)(((byte)(156)))));
            this.btn_signUp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_signUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_signUp.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_signUp.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_signUp.Location = new System.Drawing.Point(226, 440);
            this.btn_signUp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_signUp.Name = "btn_signUp";
            this.btn_signUp.Size = new System.Drawing.Size(157, 50);
            this.btn_signUp.TabIndex = 13;
            this.btn_signUp.Text = "Sign Up";
            this.btn_signUp.UseVisualStyleBackColor = false;
            this.btn_signUp.Click += new System.EventHandler(this.btn_signUp_Click);
            // 
            // txtBox_username_signUp
            // 
            this.txtBox_username_signUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_username_signUp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(68)))), ((int)(((byte)(85)))));
            this.txtBox_username_signUp.Location = new System.Drawing.Point(309, 102);
            this.txtBox_username_signUp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtBox_username_signUp.MinimumSize = new System.Drawing.Size(200, 34);
            this.txtBox_username_signUp.Name = "txtBox_username_signUp";
            this.txtBox_username_signUp.Size = new System.Drawing.Size(200, 34);
            this.txtBox_username_signUp.TabIndex = 4;
            // 
            // lbl_password_signUp
            // 
            this.lbl_password_signUp.AutoSize = true;
            this.lbl_password_signUp.BackColor = System.Drawing.Color.Transparent;
            this.lbl_password_signUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_password_signUp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(168)))), ((int)(((byte)(156)))));
            this.lbl_password_signUp.Location = new System.Drawing.Point(146, 315);
            this.lbl_password_signUp.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_password_signUp.Name = "lbl_password_signUp";
            this.lbl_password_signUp.Size = new System.Drawing.Size(139, 32);
            this.lbl_password_signUp.TabIndex = 9;
            this.lbl_password_signUp.Text = "Password";
            // 
            // txtBox_password_signUp
            // 
            this.txtBox_password_signUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_password_signUp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(68)))), ((int)(((byte)(85)))));
            this.txtBox_password_signUp.Location = new System.Drawing.Point(309, 315);
            this.txtBox_password_signUp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtBox_password_signUp.MinimumSize = new System.Drawing.Size(200, 34);
            this.txtBox_password_signUp.Name = "txtBox_password_signUp";
            this.txtBox_password_signUp.Size = new System.Drawing.Size(200, 34);
            this.txtBox_password_signUp.TabIndex = 10;
            // 
            // lbl_Username_signUp
            // 
            this.lbl_Username_signUp.AutoSize = true;
            this.lbl_Username_signUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Username_signUp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(168)))), ((int)(((byte)(156)))));
            this.lbl_Username_signUp.Location = new System.Drawing.Point(140, 102);
            this.lbl_Username_signUp.Name = "lbl_Username_signUp";
            this.lbl_Username_signUp.Size = new System.Drawing.Size(145, 32);
            this.lbl_Username_signUp.TabIndex = 3;
            this.lbl_Username_signUp.Text = "Username";
            // 
            // errorProvider_ConfirmPassw
            // 
            this.errorProvider_ConfirmPassw.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink;
            this.errorProvider_ConfirmPassw.ContainerControl = this;
            // 
            // Authentication
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Linen;
            this.ClientSize = new System.Drawing.Size(675, 648);
            this.Controls.Add(this.tab_login);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Authentication";
            this.Text = "Authentication";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Authentication_FormClosed);
            this.Load += new System.EventHandler(this.Authentication_Load);
            this.tab_login.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_ConfirmPassw)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label UserName;
        private System.Windows.Forms.Label Password;
        private System.Windows.Forms.TextBox txtBox_username;
        private System.Windows.Forms.TextBox txtBox_password;
        private System.Windows.Forms.Button btn_login;
        private System.Windows.Forms.TabControl tab_login;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btn_signUp;
        private System.Windows.Forms.TextBox txtBox_username_signUp;
        private System.Windows.Forms.Label lbl_password_signUp;
        private System.Windows.Forms.TextBox txtBox_password_signUp;
        private System.Windows.Forms.Label lbl_Username_signUp;
        private System.Windows.Forms.TextBox textBox_confirmPassword;
        private System.Windows.Forms.Label lbl_confirmPassword;
        private System.Windows.Forms.Label lbl_errorMsg_login;
        private System.Windows.Forms.ErrorProvider errorProvider_ConfirmPassw;
        private System.Windows.Forms.Label lbl_errorMsg_signup;
        private System.Windows.Forms.Label lbl_phoneNumber;
        private System.Windows.Forms.TextBox textBox_address;
        private System.Windows.Forms.Label lbl_address;
        private System.Windows.Forms.TextBox textBox_email;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.TextBox textBox_phoneNumber;
    }
}