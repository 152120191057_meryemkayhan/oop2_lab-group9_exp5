﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Lab5
{
    public class Person
    {
        public Cart cart = new Cart();

        private string username;
        private string password;
        private int ıd;
        private string email;
        private string address;
        private string phoneNumber;
        private string pathOfPP;
        private bool isAdmin;

        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public int Id { get => ıd; set => ıd = value; }
        public string Email { get => email; set => email = value; }
        public string Address { get => address; set => address = value; }
        public string PhoneNumber { get => phoneNumber; set => phoneNumber = value; }
        public string PathOfPP { get => pathOfPP; set => pathOfPP = value; }
        public bool IsAdmin { get => isAdmin; set => isAdmin = value; }
        public Person() { }
        public Person(int ID, string Username, string Email, string Password, string Address, string PhoneNumber, string pp, bool IsAdmin)
        {
            this.username = Username;
            this.password = Password;
            this.isAdmin = IsAdmin;
            this.Id = ID;
            this.email = Email;
            this.address = Address;
            this.phoneNumber = PhoneNumber;
            this.pathOfPP = pp;
        }
    }
}

