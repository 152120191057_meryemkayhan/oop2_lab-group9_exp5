﻿using OOP_Lab5;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareLib
{
     /// <summary>
     /// This class compares customer accounts with one another to see if there already exists an account with that email.
     /// And other processes like that.
     /// It helps with keeping the customer accounts unique.
     /// </summary>
    public class Compare
    {
        // formdan alınan şifrenin people listi içinde tutulan person kullanıcılarından herhangi biriyle eşleşip eşleşmediğine
        // eşleşiyor ise adminle mi yoksa adminin, sayelerinde para kazanacağı müşterilerle mi eşleştiğine baktığımız fonksiyon aşağıdadır.
        public static int detect_PermissionLevel(List<Person> People, string TxtInput_username, string TxtInput_password)
        {
            string hashedPsw = CryptoLib.Encryptor.MD5Hash(TxtInput_password);

            for (int i = 0; i < People.Count; i++)
            {
                if (TxtInput_username == People[i].Username)
                {
                    if (hashedPsw == People[i].Password)
                    {
                        if (People[i].IsAdmin == false) return 1; // access permission level = 1 means the user is çinko karbon vatandaş
                        else return 2; // permission of access level is 2 means the user is admin.
                    }
                }


            }
            return 0; // şifrsini yanlış giren dikkatsiz bir kullanıcı. kapıdan göndermek için 0 return ediyoruz.
        }

        // giriş yapmaya çalışan insancığın kayıtlı kullanıcı olup olmadığına baktığımız fonksiyonumuz:
        public static bool detect_usernameTaken(List<Person> People, string TxtInput_username)
        {
            for (int i = 0; i < People.Count; i++)
            {
                if (TxtInput_username == People[i].Username) return true;
            }
            return false;
        }

        // registered email cheeeeck
        public static bool detect_emailTaken(List<Person> People, string TxtInput_email)
        {
            for (int i = 0; i < People.Count; i++)
            {
                if (TxtInput_email == People[i].Email) return true;
            }
            return false;
        }

        ///girilen iki metin kusundaki stringlerin aynı olup olmadığını kontrol eder.
        public static bool confirmPassword(string _old, string _new)
        {
            if (_old == _new) return true;
            else return false;
        }
    }
}
