﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP_Lab5
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Authentication aut = new Authentication();
            Store str = new Store();
            ProductDescription prdctDesc = new ProductDescription();
            Application.Run(aut);    
            if (aut.isLoginClicked)
                Application.Run(new Store());
            if (str.isSignOutClicked)
                Application.Run(aut);
        }
    }
}