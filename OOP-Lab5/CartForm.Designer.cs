﻿namespace OOP_Lab5
{
    partial class CartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lv_Cart = new System.Windows.Forms.ListView();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.btnArti = new System.Windows.Forms.Button();
            this.btnEksi = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lblError = new System.Windows.Forms.Label();
            this.btnGoback = new System.Windows.Forms.Button();
            this.btnBuy = new System.Windows.Forms.Button();
            this.btnClearCart = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lv_Cart
            // 
            this.lv_Cart.FullRowSelect = true;
            this.lv_Cart.HideSelection = false;
            this.lv_Cart.Location = new System.Drawing.Point(12, 20);
            this.lv_Cart.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lv_Cart.Name = "lv_Cart";
            this.lv_Cart.Size = new System.Drawing.Size(428, 378);
            this.lv_Cart.TabIndex = 0;
            this.lv_Cart.UseCompatibleStateImageBehavior = false;
            this.lv_Cart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lv_Cart_MouseClick);
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(489, 52);
            this.txtAmount.Margin = new System.Windows.Forms.Padding(4);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.ReadOnly = true;
            this.txtAmount.Size = new System.Drawing.Size(52, 22);
            this.txtAmount.TabIndex = 1;
            // 
            // btnArti
            // 
            this.btnArti.BackColor = System.Drawing.Color.AliceBlue;
            this.btnArti.ForeColor = System.Drawing.Color.Purple;
            this.btnArti.Location = new System.Drawing.Point(514, 82);
            this.btnArti.Margin = new System.Windows.Forms.Padding(4);
            this.btnArti.Name = "btnArti";
            this.btnArti.Size = new System.Drawing.Size(27, 25);
            this.btnArti.TabIndex = 2;
            this.btnArti.Text = "+";
            this.btnArti.UseVisualStyleBackColor = false;
            this.btnArti.Click += new System.EventHandler(this.btnArti_Click);
            // 
            // btnEksi
            // 
            this.btnEksi.BackColor = System.Drawing.Color.AliceBlue;
            this.btnEksi.ForeColor = System.Drawing.Color.Purple;
            this.btnEksi.Location = new System.Drawing.Point(489, 82);
            this.btnEksi.Margin = new System.Windows.Forms.Padding(4);
            this.btnEksi.Name = "btnEksi";
            this.btnEksi.Size = new System.Drawing.Size(27, 25);
            this.btnEksi.TabIndex = 3;
            this.btnEksi.Text = "-";
            this.btnEksi.UseVisualStyleBackColor = false;
            this.btnEksi.Click += new System.EventHandler(this.btnEksi_Click_1);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.AliceBlue;
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnUpdate.ForeColor = System.Drawing.Color.Purple;
            this.btnUpdate.Location = new System.Drawing.Point(472, 129);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(4);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(123, 43);
            this.btnUpdate.TabIndex = 4;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.AliceBlue;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnDelete.ForeColor = System.Drawing.Color.Purple;
            this.btnDelete.Location = new System.Drawing.Point(472, 180);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(123, 43);
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Text = "Remove";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(468, 20);
            this.lblError.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 17);
            this.lblError.TabIndex = 6;
            // 
            // btnGoback
            // 
            this.btnGoback.BackColor = System.Drawing.Color.AliceBlue;
            this.btnGoback.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnGoback.ForeColor = System.Drawing.Color.Purple;
            this.btnGoback.Location = new System.Drawing.Point(504, 359);
            this.btnGoback.Margin = new System.Windows.Forms.Padding(4);
            this.btnGoback.Name = "btnGoback";
            this.btnGoback.Size = new System.Drawing.Size(91, 53);
            this.btnGoback.TabIndex = 7;
            this.btnGoback.Text = "Go Back";
            this.btnGoback.UseVisualStyleBackColor = false;
            this.btnGoback.Click += new System.EventHandler(this.btnGoback_Click);
            // 
            // btnBuy
            // 
            this.btnBuy.BackColor = System.Drawing.Color.AliceBlue;
            this.btnBuy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnBuy.ForeColor = System.Drawing.Color.Purple;
            this.btnBuy.Location = new System.Drawing.Point(471, 231);
            this.btnBuy.Margin = new System.Windows.Forms.Padding(4);
            this.btnBuy.Name = "btnBuy";
            this.btnBuy.Size = new System.Drawing.Size(123, 43);
            this.btnBuy.TabIndex = 8;
            this.btnBuy.Text = "Buy";
            this.btnBuy.UseVisualStyleBackColor = false;
            this.btnBuy.Click += new System.EventHandler(this.btnBuy_Click_1);
            // 
            // btnClearCart
            // 
            this.btnClearCart.BackColor = System.Drawing.Color.AliceBlue;
            this.btnClearCart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnClearCart.ForeColor = System.Drawing.Color.Purple;
            this.btnClearCart.Location = new System.Drawing.Point(472, 281);
            this.btnClearCart.Name = "btnClearCart";
            this.btnClearCart.Size = new System.Drawing.Size(123, 43);
            this.btnClearCart.TabIndex = 9;
            this.btnClearCart.Text = "Clear Cart";
            this.btnClearCart.UseVisualStyleBackColor = false;
            this.btnClearCart.Click += new System.EventHandler(this.btnClearCart_Click);
            // 
            // CartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Linen;
            this.ClientSize = new System.Drawing.Size(611, 427);
            this.Controls.Add(this.btnClearCart);
            this.Controls.Add(this.btnBuy);
            this.Controls.Add(this.btnGoback);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnEksi);
            this.Controls.Add(this.btnArti);
            this.Controls.Add(this.txtAmount);
            this.Controls.Add(this.lv_Cart);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CartForm";
            this.Text = "CartForm";
            this.Load += new System.EventHandler(this.CartForm_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lv_Cart;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.Button btnArti;
        private System.Windows.Forms.Button btnEksi;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Button btnGoback;
        private System.Windows.Forms.Button btnBuy;
        private System.Windows.Forms.Button btnClearCart;
    }
}