﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using OOP_Lab5;

namespace OOP_Lab5
{
    public partial class ProductDescription : Form
    {
        String connectionstring = "Data Source=SQL5063.site4now.net;Initial Catalog=db_a75584_ooplab;User Id=db_a75584_ooplab_admin;Password=7eQ2icRhm_au";
        SqlConnection connection;
        SqlDataAdapter adapter;
        SqlCommand command;

        string pr_name = "";
        string pr_price = "";
        int pr_id = 0;
        string path = "";
        int user_id = 0;
        string pr_desc = "";

        public void setPath(string path_) { path = path_; }
        public void setName(string name) { pr_name = name; }
        public void setPrice(string price) { pr_price = price; }
        public void setDesc(string desc) { pr_desc = desc; }
        public void setProID(int id) { pr_id = id; }
        public void setUserID(int id) { user_id = id; }
        public ProductDescription()
        {
            InitializeComponent();
        }
        public Person loggedPerson = new Person();
        public void setPerson(Person autPerson)
        {
            loggedPerson = autPerson;
            loggedPerson.Id = autPerson.Id;
            loggedPerson.cart = autPerson.cart;
        }
        private void ProductDescription_Load(object sender, EventArgs e)
        {
            pictureBox1.ImageLocation = path;

            for (int i = 0; i < AdminTab.GetForm.size(); i++)
            {
                if (pr_name == AdminTab.GetForm.GetListElement(i)[0].ToString())
                {
                    lblName.Text = "Product: " + pr_name;
                    lblDesc.Text = "Description: " + pr_desc;
                    lblPrice.Text = "Price: " + pr_price;
                }
            }

        }
        private void btnGoBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnAddToCart_Click(object sender, EventArgs e)
        {
            if (txtAmountRuler() == true)
            {
                connection = new SqlConnection(connectionstring);
                connection.Open();

                string input = "Insert Into ShoppingCart(UserID, ProductID, Amount) values (@UserID, @ProductID, @Amount)";
                string selectID = "Select * from ShoppingCart";

                adapter = new SqlDataAdapter(selectID, connection);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);

                bool flag = false;
                int dbuserid = 0;
                int appproid = 0;
                int dbproid = 0;
                int appuserid = 0;
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    dbproid = Convert.ToInt32(dataTable.Rows[i]["ProductID"]);
                    appproid = int.Parse(AdminTab.GetForm.GetListElement(pr_name)[0]);
                    dbuserid = Convert.ToInt32(dataTable.Rows[i]["UserID"]);
                    appuserid = user_id;

                    if (loggedPerson.Id == dbuserid && appproid == dbproid)
                    {
                        string updateS = "Update ShoppingCart set Amount = @Amount where UserID ='" + dbuserid + "' AND ProductID =" + dbproid;

                        SqlCommand update = new SqlCommand(updateS, connection);
                        if (Convert.ToInt32(txtAmount.Text.ToString()) < 1)
                        {
                            lblError.Text = "Amount cannot be smaller than 1.\nYour order will be processed with 1 amount.";
                            txtAmount.Text = "1";
                        }
                        int quanty = (Convert.ToInt32(dataTable.Rows[i]["Amount"]) + Convert.ToInt32(txtAmount.Text));
                        update.Parameters.AddWithValue("@Amount", quanty);
                        update.ExecuteNonQuery();
                        flag = true;
                    }
                }
                if (flag == false)
                {
                    command = new SqlCommand(input, connection);
                    command.Parameters.Add("@UserID",loggedPerson.Id);
                    command.Parameters.Add("@productID", pr_id);
                    if (Convert.ToInt32(txtAmount.Text.ToString()) < 1)
                    {
                        lblError.Text = "Amount cannot be smaller than 1.\nYour order will be processed with 1 amount.";
                        txtAmount.Text = "1";
                    }
                    command.Parameters.Add("@Amount", Convert.ToInt32(txtAmount.Text));
                    command.ExecuteNonQuery();
                }
                lb2.Text = "Product has been added.";
                connection.Close();
            }
        }     
        private bool txtAmountRuler()
        {
            if (txtAmount.Text == "")
            {
                lblError.Text = "You must \nenter amount";
                return false;
            }
            else if (!int.TryParse(txtAmount.Text, out _))
            {
                lblError.Text = "Your amount must \nbe a number";
                return false;
            }
            else if (int.TryParse(txtAmount.Text, out _))
            {
                if (int.Parse(txtAmount.Text) > 100)
                {
                    lblError.Text = "Your amount must be \nless than 100";
                    return false;
                }
            }
            return true;
        }
    }
}
