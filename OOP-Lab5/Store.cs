﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.IO;
using System.Drawing.Imaging;
using OOP_Lab5;
/*
 * admin username and password:
 * admin, password
 * customer usernames and passwords: 
 * ayraso, 0000
 * orcun, 55555
 * emin, 7777
 * mer, 1234
 * 
 * SQL server name: SQL5063.site4now.net
 * SQL username: db_a75584_ooplab_admin
 * password: 7eQ2icRhm_au
 * 
 * */
namespace OOP_Lab5
{
    public partial class Store : Form
    {
        String connectionstring = "Data Source=SQL5063.site4now.net;Initial Catalog=db_a75584_ooplab;User Id=db_a75584_ooplab_admin;Password=7eQ2icRhm_au";
        SqlConnection connection;
        SqlDataAdapter adapter;
        Person loggedPerson = new Person();
        Profile profileForm = new Profile();
        public void setPerson(Person autPerson)
        {
            loggedPerson = autPerson;
            loggedPerson.Id = autPerson.Id;
            loggedPerson.cart = autPerson.cart;
        }
        public bool isSignOutClicked { get; set; }
        
        public Store()
        {
            InitializeComponent();
            Person loggedPerson = new Person();
        }
        private void Store_Load(object sender, EventArgs e)
        {
            connection = new SqlConnection(connectionstring);
            listView1.View = View.LargeIcon;
            listView1.Columns.Add("Products", 150, HorizontalAlignment.Center);
            listView1.AutoResizeColumn(0, ColumnHeaderAutoResizeStyle.HeaderSize);
            populate();         
        }
        private void populate()
        {
            ImageList imgs = new ImageList();
            imgs.ColorDepth = ColorDepth.Depth32Bit;
            imgs.ImageSize = new Size(100, 100);
            try
            {
                for (int i = 0; i < AdminTab.GetForm.size(); i++) 
                {
                    Image imgProduct = new Bitmap(AdminTab.GetForm.GetImageProperty(i));
                            imgs.Images.Add(imgProduct);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            listView1.LargeImageList = imgs;

            for (int i = 0; i < AdminTab.GetForm.size(); i++)
            {
                listView1.Items.Add(new ListViewItem(AdminTab.GetForm.GetListElement(i)[0].ToString(), i));
            }
        }
        private void btnSignOut_Click(object sender, EventArgs e)
        {
            isSignOutClicked = true;
            this.Close();
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnCart_Click(object sender, EventArgs e)
        {
            CartForm cartform = new CartForm();
            cartform.setPerson(loggedPerson);
            cartform.Show();        
        }
        private void Store_FormClosed(object sender, FormClosedEventArgs e)
        {         
            if (e.CloseReason == CloseReason.UserClosing)
            {
                int no = 0;
                no = Convert.ToInt32(loggedPerson.Id);
                if (no==5)
                    AdminTab.GetForm.Show();
                else
                    Authentication.GetForm.Show();
            }               
        }
        private void listView1_MouseClick(object sender, MouseEventArgs e)
        {
            ProductDescription proDesc = new ProductDescription();
            string prodName = listView1.FocusedItem.SubItems[0].Text.ToString();
            string path = "";
            string pr_price = "";
            int pr_id = 0;
            string desc = "";
            int userid = 0;
            userid = loggedPerson.Id;
            string selectsentence = "Select * from Product";
            adapter = new SqlDataAdapter(selectsentence, connection);
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);
            connection.Open();
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                if (prodName == dataTable.Rows[i]["Name"].ToString())
                {
                    pr_id = Convert.ToInt32(dataTable.Rows[i]["ProductID"]);
                    pr_price = dataTable.Rows[i]["Price"].ToString();
                    path = dataTable.Rows[i]["Path"].ToString();
                    desc= dataTable.Rows[i]["Description"].ToString();
                    break;
                }
            }
            proDesc.loggedPerson = loggedPerson;
            proDesc.setName(prodName);
            proDesc.setProID(pr_id);
            proDesc.setPrice(pr_price);
            proDesc.setPath(path);
            proDesc.setDesc(desc);
            proDesc.setUserID(userid);
            proDesc.Show();
            connection.Close();
        }
        private void btn_profile_Click(object sender, EventArgs e)
        {
            this.Hide();
            profileForm.setPerson(loggedPerson);
            profileForm.Show();
        }
    }
}
