﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using OOP_Lab5;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Drawing;

namespace OOP_Lab5
{
    public class Data
    {
        List<Node> DataList = new List<Node>();

        private static Data inst;

        public static Data GetForm
        {
            get
            {
                if (inst == null)
                    inst = new Data();
                return inst;

            }
        }
        public Data()
        {
            string fileName = "elma.jpg";
            string path = Path.Combine(Environment.CurrentDirectory, @"pictures\", fileName);
            Node node = new Node("Elma", "5", "kg", fileName, path);
            DataList.Add(node);

            fileName = "kivi.jpg";
            path = Path.Combine(Environment.CurrentDirectory, @"pictures\", fileName);
            node = new Node("Kivi", "20", "kg", fileName, path);
            DataList.Add(node);

            fileName = "incir.jpg";
            path = Path.Combine(Environment.CurrentDirectory, @"pictures\", fileName);
            node = new Node("İncir", "12", "kg", fileName, path);
            DataList.Add(node);

            fileName = "armut.jpg";
            path = Path.Combine(Environment.CurrentDirectory, @"pictures\", fileName);
            node = new Node("Armut", "11", "kg", fileName, path);
            DataList.Add(node);

            fileName = "portakal.jpg";
            path = Path.Combine(Environment.CurrentDirectory, @"pictures\", fileName);
            node = new Node("Portakal", "4", "kg", fileName, path);
            DataList.Add(node);

            fileName = "üzüm.jpg";
            path = Path.Combine(Environment.CurrentDirectory, @"pictures\", fileName);
            node = new Node("Üzüm", "8", "kg", fileName, path);
            DataList.Add(node);

            fileName = "karpuz.jpg";
            path = Path.Combine(Environment.CurrentDirectory, @"pictures\", fileName);
            node = new Node("Karpuz", "2", "kg", fileName, path);
            DataList.Add(node);

            fileName = "çilek.jpg";
            path = Path.Combine(Environment.CurrentDirectory, @"pictures\", fileName);
            node = new Node("Çilek", "13", "kg", fileName, path);
            DataList.Add(node);

            fileName = "erik.jpg";
            path = Path.Combine(Environment.CurrentDirectory, @"pictures\", fileName);
            node = new Node("Erik", "32", "kg", fileName, path);
            DataList.Add(node);

            fileName = "muz.jpg";
            path = Path.Combine(Environment.CurrentDirectory, @"pictures\", fileName);
            node = new Node("Muz", "12", "kg", fileName, path);
            DataList.Add(node);

        }

        public bool Add(string Name, string Price, string Description, string FileName, string Path)
        {
            for(int i=0;i<DataList.Count;i++)
            {
                if (DataList[i].name==Name)
                    return false;
            }
            if (Name != "" && Price != "" && Description != "" && FileName != "" && Path != "")
            {
                Node node = new Node(Name, Price, Description, FileName, Path);
                DataList.Add(node);
                return true;
            }
            else
                return false;
        }

        public bool Remove(string Name)
        {
            if (DataList.Remove(DataList.Find(x => x.name.Equals(Name))))
                return true;
            else
                return false;
        }

        public bool Update(string Name, string Price, string Description, string FileName, string Path, string nodename,bool chcboxChecked)
        {
           
            int idx = DataList.FindIndex(x => x.name==(nodename));
            var temp = DataList[idx];
            bool updated = false;
            if (temp.name != null){
                if(Name != "" && Name != temp.name)
                {
                    temp.name = Name;
                    updated = true;
                }   
                if(Price != "" && Price != temp.price) 
                {
                    temp.price = Price;
                    updated = true;
                }
                if(Description != "" && Description != temp.description) 
                {
                    temp.description = Description;
                    updated = true;
                }
            }
            
            if(Path != "" && FileName != "" && chcboxChecked==true)
            {
                temp.fileName = FileName;
                temp.path = Path;
                updated = true;
            }
            DataList[idx] = temp;
            return updated;
        }

        public string[] GetListElement(int index)
        {
            string[] liste = { DataList[index].name, DataList[index].price, DataList[index].description, DataList[index].path };

            return liste;
        }
        public string[] GetListElement(string name)
        {
            int index = DataList.FindIndex(x => x.name == (name));
            string[] liste = { DataList[index].name, DataList[index].price, DataList[index].description };
            return liste;
        }
        public int size()
        {
            return DataList.Count;
        }
        public string[] GetImageProperty(string name)
        {
            int index = DataList.FindIndex(x => x.name == (name));
            string[] temp = { DataList[index].path, DataList[index].fileName };
            return temp;
        }
    }
}
