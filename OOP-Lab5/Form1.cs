﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OOP_Lab5;
using System.IO;

namespace OOP_Lab5
{
    public partial class Form1 : Form
    {
        Image image;
        Data data = new Data();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listView1.View = View.Details;
            listView1.Columns.Add("Product", 75);
            listView1.Columns.Add("Price", 75);
            listView1.Columns.Add("Description", 75);
            listView1.Columns.Add("Path", 150);


            string[] information = data.GetListElement(0);
            ListViewItem item = new ListViewItem(information);
            listView1.Items.Add(item);

            information = data.GetListElement(1);
            item = new ListViewItem(information);
            listView1.Items.Add(item);

            information = data.GetListElement(2);
            item = new ListViewItem(information);
            listView1.Items.Add(item);

            information = data.GetListElement(3);
            item = new ListViewItem(information);
            listView1.Items.Add(item);

            information = data.GetListElement(4);
            item = new ListViewItem(information);
            listView1.Items.Add(item);

            information = data.GetListElement(5);
            item = new ListViewItem(information);
            listView1.Items.Add(item);

            information = data.GetListElement(6);
            item = new ListViewItem(information);
            listView1.Items.Add(item);

            information = data.GetListElement(7);
            item = new ListViewItem(information);
            listView1.Items.Add(item);

            information = data.GetListElement(8);
            item = new ListViewItem(information);
            listView1.Items.Add(item);

            information = data.GetListElement(9);
            item = new ListViewItem(information);
            listView1.Items.Add(item);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string fileName = "";// = txtFilename.Text;
            string path = "";// Path.Combine(Environment.CurrentDirectory, @"pictures\", fileName);
            string directory = "";
            bool picturestate=false;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                directory = openFileDialog1.FileName;
                fileName = directory.Substring(directory.LastIndexOf(@"\") + 1);
                path = Path.Combine(Environment.CurrentDirectory, @"pictures\", fileName);
                picturestate = true;
            }

            if (data.Add(txtName.Text, txtPrice.Text, txtDesc.Text, fileName/*txtFilename.Text*/, path/*txtPath.Text*/) && picturestate == true)
            {
                image = Image.FromFile(openFileDialog1.FileName);
                if(openFileDialog1.FileName != path)
                    image.Save(path);

                string[] information = { txtName.Text, txtPrice.Text, txtDesc.Text, path };
                ListViewItem newAdd = new ListViewItem(information);

                listView1.Items.Add(newAdd);
                lblError.Text = "";
            }
            else if(picturestate==false)
                lblError.Text = "There is no selected image to add";
            else
                lblError.Text = "Invalid informations.";
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            string fileName = "";// = txtFilename.Text;
            string path = "";// Path.Combine(Environment.CurrentDirectory, @"pictures\", fileName);
            string directory = "";
            bool picturestate = false;
            lblError.Text = "";
            int flag = 0;
            if (listView1.SelectedItems.Count > 0)
            {
                if (chcboxPicture.Checked == true)
                {
                    if (openFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        directory = openFileDialog1.FileName;
                        fileName = directory.Substring(directory.LastIndexOf(@"\") + 1);
                        path = Path.Combine(Environment.CurrentDirectory, @"pictures\", fileName);
                        picturestate = true;
                        flag = 1;
                    }
                    else
                    {
                        directory = openFileDialog1.FileName;
                        picturestate = false;
                    }
                }

                if (data.Update(txtName.Text, txtPrice.Text, txtDesc.Text, fileName, path, listView1.SelectedItems[0].SubItems[0].Text, chcboxPicture.Checked))
                {
                    if (chcboxPicture.Checked == true)
                    {
                        if (flag==1)
                        {
                            image = Image.FromFile(openFileDialog1.FileName);
                            if (openFileDialog1.FileName != path)
                                image.Save(path);
                        }
                        else
                        {
                            lblError.Text = "Picture path has not changed.";
                        }
                    }
                    if (txtName.Text != "")
                        listView1.SelectedItems[0].SubItems[0].Text = txtName.Text;
                    if (txtPrice.Text != "")
                        listView1.SelectedItems[0].SubItems[1].Text = txtPrice.Text;
                    if (txtDesc.Text != "")
                        listView1.SelectedItems[0].SubItems[2].Text = txtDesc.Text;
                    if (chcboxPicture.Checked == true)
                        listView1.SelectedItems[0].SubItems[3].Text = path;
                }
                else if (chcboxPicture.Checked == true && picturestate == false)
                    lblError.Text = "There is no selected image to update";
                else if (txtName.Text == listView1.SelectedItems[0].SubItems[0].Text && txtPrice.Text == listView1.SelectedItems[0].SubItems[1].Text && txtDesc.Text == listView1.SelectedItems[0].SubItems[2].Text)
                    lblError.Text = "Any value is not changed.";
                else
                    lblError.Text = "Invalid  input";
                if ((txtName.Text == "" || txtPrice.Text == "" || txtDesc.Text == "") && chcboxPicture.Checked == true && picturestate == true) 
                    lblError.Text = "Picture updated but informations didn't update \n because of invalid input.";
                pictureBox1.ImageLocation = listView1.FocusedItem.SubItems[3].Text.ToString();
            }
            else
                lblError.Text = "There is no selected item to update.";
        }
        
            

        private void btnDelete_Click(object sender, EventArgs e)
        {
            
            if (data.Remove(txtName.Text))
            {
                listView1.Items.RemoveAt(listView1.SelectedIndices[0]);
                txtName.Text = "";
                txtDesc.Text = "";
                txtPrice.Text = "";
               // pictureBox1.Image=null;
            }
            else
                lblError.Text = "There is not a/n element which has/n entered name.";
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            pictureBox1.ImageLocation = listView1.FocusedItem.SubItems[3].Text.ToString();

            txtName.Text = listView1.FocusedItem.SubItems[0].Text.ToString();
            txtPrice.Text = listView1.FocusedItem.SubItems[1].Text.ToString();
            txtDesc.Text = listView1.FocusedItem.SubItems[2].Text.ToString();
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDesc_TextChanged(object sender, EventArgs e)
        {

        }

        private void chcboxPicture_CheckedChanged(object sender, EventArgs e)
        {
           
        }
    }
}
