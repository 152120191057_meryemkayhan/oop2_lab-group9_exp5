﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

namespace OOP_Lab5
{
    public partial class CartForm : Form
    {
        Person loggedPerson;
        bool isSelectedNull = true;

        static string constring = "Data Source = SQL5063.site4now.net; Initial Catalog = db_a75584_ooplab; User Id = db_a75584_ooplab_admin; Password=7eQ2icRhm_au";
        SqlConnection connection;
        SqlCommand command;
        SqlDataAdapter adapter;
        Cart cart = new Cart();
        public void setPerson(Person autPerson)
        {
            loggedPerson = autPerson;
            loggedPerson.Id = autPerson.Id;
            loggedPerson.cart = autPerson.cart;
        }    
        public CartForm()
        {
            loggedPerson = new Person();
            InitializeComponent();
        }
        private void CartForm_Load_1(object sender, EventArgs e)
        {
            populate();
        }
        private void populate()
        {
            lv_Cart.View = View.Details;
            lv_Cart.Columns.Add("Product", 100);
            lv_Cart.Columns.Add("Amount", 50);
            lv_Cart.Columns.Add("Total Price", 100);
            ImageList imgs = new ImageList();
            imgs.ImageSize = new Size(32, 32);
            
            connection = new SqlConnection(constring);
            connection.Open();
            string selectProductID = "Select * from ShoppingCart where UserID = '" + loggedPerson.Id + "'";
            adapter = new SqlDataAdapter(selectProductID, connection);
            DataTable ProductIDdataTable = new DataTable();
            adapter.Fill(ProductIDdataTable);
            
            for (int i = 0; i < ProductIDdataTable.Rows.Count; i++)
            {
                DataTable pathsData = new DataTable();
                SqlDataAdapter pathAdapter;
                string selectProductPath = "Select * from Product where ProductID = '" + ProductIDdataTable.Rows[i]["ProductID"] +"'";
                pathAdapter = new SqlDataAdapter(selectProductPath, connection);
                pathAdapter.Fill(pathsData);
                imgs.Images.Add(Image.FromFile(Path.Combine(Environment.CurrentDirectory, pathsData.Rows[0]["Path"].ToString())));
            }

            lv_Cart.SmallImageList = imgs;

            for (int i = 0; i < ProductIDdataTable.Rows.Count; i++)
            {
                DataTable productNameData = new DataTable();
                SqlDataAdapter NameAdapter;
                string selectProductName = "Select * from Product where ProductID = '" + ProductIDdataTable.Rows[i]["ProductID"] + "'";
                NameAdapter = new SqlDataAdapter(selectProductName, connection);
                NameAdapter.Fill(productNameData);

                ListViewItem lst = new ListViewItem(productNameData.Rows[0]["Name"].ToString(), i);
                int amount = Convert.ToInt32(ProductIDdataTable.Rows[i]["Amount"].ToString());
                int totalPrice = Convert.ToInt32(productNameData.Rows[0]["Price"].ToString());
                lst.SubItems.Add(amount.ToString());
                lst.SubItems.Add((amount * totalPrice).ToString());
                lv_Cart.Items.Add(lst);
            }
            connection.Close();
        }
        private void lv_Cart_MouseClick(object sender, MouseEventArgs e)
        {
            if (lv_Cart.FocusedItem == null)
            {
                isSelectedNull = true;
            }
            else
            {
                isSelectedNull = false;
                txtAmount.Text = lv_Cart.FocusedItem.SubItems[1].Text;
                lblError.Text = "";
            }
        }
        private void btnArti_Click(object sender, EventArgs e)
        {
            if (!isSelectedNull)
            {
                int temp = Int32.Parse(txtAmount.Text);
                temp++;
                txtAmount.Text = temp.ToString();
            }
        }
        

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!isSelectedNull && lv_Cart.Items.Count > 0)
            {
                if (txtAmount.Text == lv_Cart.FocusedItem.SubItems[1].Text)
                    lblError.Text = "You didn't change amount.";
                else
                {
                    connection = new SqlConnection(constring);
                    connection.Open();

                    string selectProduct = "Select * from Product where Name = '" + lv_Cart.FocusedItem.SubItems[0].Text.ToString() + "'";
                    SqlDataAdapter selAdapter = new SqlDataAdapter(selectProduct, connection);
                    DataTable selDataTable = new DataTable();
                    selAdapter.Fill(selDataTable);             
                    string change = "Update ShoppingCart set Amount = @amount where UserID = '" + loggedPerson.Id + "' AND ProductID = '" + selDataTable.Rows[0]["ProductID"].ToString() + "'";
                    SqlCommand update = new SqlCommand(change, connection);
                    int quanty = Convert.ToInt32(txtAmount.Text.ToString());
                    update.Parameters.AddWithValue("@amount", quanty);
                    update.ExecuteNonQuery();

                    lv_Cart.FocusedItem.SubItems[1].Text = txtAmount.Text;
                    lv_Cart.FocusedItem.SubItems[2].Text = (Convert.ToInt32(txtAmount.Text.ToString()) * Convert.ToInt32(selDataTable.Rows[0]["Price"].ToString())).ToString();
                    lblError.Text = "";
                    connection.Close();
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            connection = new SqlConnection(constring);
            connection.Open();

            string name = lv_Cart.FocusedItem.SubItems[0].Text;
            string selectProduct = "Select * from Product where Name = '" + lv_Cart.FocusedItem.SubItems[0].Text.ToString() + "'";
            adapter = new SqlDataAdapter(selectProduct, connection);
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);
            try
            {
                string SelectDelete = "Delete from ShoppingCart where UserID = '" + loggedPerson.Id + "' and ProductID = '" + dataTable.Rows[0]["ProductID"] + "'";
                command = new SqlCommand(SelectDelete, connection);
                command.ExecuteNonQuery();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            connection.Close();
            lv_Cart.FocusedItem.Remove();
            txtAmount.Text = "";
            isSelectedNull = true;
        }
        private void btnGoback_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEksi_Click_1(object sender, EventArgs e)
        {
            if (!isSelectedNull)
            {
                int temp = Int32.Parse(txtAmount.Text);
                if (temp > 1)
                    temp--;
                else
                    lblError.Text = "Amount can't be\n less than 1.";
                txtAmount.Text = temp.ToString();
            }
        }

        private void btnBuy_Click_1(object sender, EventArgs e)
        {
            connection = new SqlConnection(constring);
            connection.Open();

            string SelectDelete = "Delete from ShoppingCart where UserID = '" + loggedPerson.Id + "'";
            command = new SqlCommand(SelectDelete, connection);
            command.ExecuteNonQuery();
            connection.Close();
            lv_Cart.Clear();
            txtAmount.Text = "";
            MessageBox.Show("Your order will be processed.");
        }
        private void btnClearCart_Click(object sender, EventArgs e)
        {
            int cust_id = 0;
            cust_id = loggedPerson.Id;
            cart.clearMyCart(cust_id);
            lv_Cart.Items.Clear();
        }
    }
}
