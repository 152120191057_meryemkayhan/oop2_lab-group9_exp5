﻿
namespace OOP_Lab5
{
    partial class Profile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Profile));
            this.btn_goShop = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabProfil = new System.Windows.Forms.TabPage();
            this.btn_saveChanges = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lbl_username = new System.Windows.Forms.Label();
            this.lbl_email = new System.Windows.Forms.Label();
            this.pnl_changePassword2 = new System.Windows.Forms.Panel();
            this.lbl_error_password = new System.Windows.Forms.Label();
            this.txtbox_newPassword = new System.Windows.Forms.TextBox();
            this.txtbox_oldPassword = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_error_email = new System.Windows.Forms.Label();
            this.txtbox_email = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbl_error_username = new System.Windows.Forms.Label();
            this.txtbox_username = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lbl_error_nameSurname = new System.Windows.Forms.Label();
            this.txtbox_address = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtPhoneNumber0 = new System.Windows.Forms.TextBox();
            this.lbl_error_address = new System.Windows.Forms.Label();
            this.pnl_changePassword1 = new System.Windows.Forms.Panel();
            this.lbl_newPassword = new System.Windows.Forms.Label();
            this.lbl_oldPassword = new System.Windows.Forms.Label();
            this.lbl_changePassword = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblPhoneNumber = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnChangeProPic = new System.Windows.Forms.Button();
            this.btnRemoveProPic = new System.Windows.Forms.Button();
            this.btnDeleteAccount = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabProfil.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.pnl_changePassword2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.pnl_changePassword1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_goShop
            // 
            this.btn_goShop.BackColor = System.Drawing.Color.AliceBlue;
            this.btn_goShop.ForeColor = System.Drawing.Color.Purple;
            this.btn_goShop.Location = new System.Drawing.Point(879, 597);
            this.btn_goShop.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_goShop.Name = "btn_goShop";
            this.btn_goShop.Size = new System.Drawing.Size(143, 45);
            this.btn_goShop.TabIndex = 18;
            this.btn_goShop.Text = "Go Shop";
            this.btn_goShop.UseVisualStyleBackColor = false;
            this.btn_goShop.Click += new System.EventHandler(this.btn_goShop_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabProfil);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.tabControl1.Location = new System.Drawing.Point(12, 11);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(715, 641);
            this.tabControl1.TabIndex = 2;
            // 
            // tabProfil
            // 
            this.tabProfil.BackColor = System.Drawing.Color.Linen;
            this.tabProfil.Controls.Add(this.btn_saveChanges);
            this.tabProfil.Controls.Add(this.tableLayoutPanel1);
            this.tabProfil.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.tabProfil.Location = new System.Drawing.Point(4, 34);
            this.tabProfil.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabProfil.Name = "tabProfil";
            this.tabProfil.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabProfil.Size = new System.Drawing.Size(707, 603);
            this.tabProfil.TabIndex = 0;
            this.tabProfil.Text = "                        Profile                        ";
            // 
            // btn_saveChanges
            // 
            this.btn_saveChanges.BackColor = System.Drawing.Color.White;
            this.btn_saveChanges.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_saveChanges.BackgroundImage")));
            this.btn_saveChanges.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_saveChanges.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_saveChanges.ForeColor = System.Drawing.Color.Sienna;
            this.btn_saveChanges.Location = new System.Drawing.Point(289, 529);
            this.btn_saveChanges.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_saveChanges.Name = "btn_saveChanges";
            this.btn_saveChanges.Size = new System.Drawing.Size(68, 63);
            this.btn_saveChanges.TabIndex = 13;
            this.btn_saveChanges.Text = "Save";
            this.btn_saveChanges.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_saveChanges.UseVisualStyleBackColor = false;
            this.btn_saveChanges.Click += new System.EventHandler(this.btn_saveChanges_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.Controls.Add(this.panel6, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl_username, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl_email, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pnl_changePassword2, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.pnl_changePassword1, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 0, 3);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 5);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66852F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66852F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66852F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.79693F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.20307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(701, 522);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.lblAddress);
            this.panel6.Location = new System.Drawing.Point(3, 177);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(274, 81);
            this.panel6.TabIndex = 9;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(87, 28);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(83, 23);
            this.lblAddress.TabIndex = 4;
            this.lblAddress.Text = "Address";
            // 
            // lbl_username
            // 
            this.lbl_username.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_username.AutoSize = true;
            this.lbl_username.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_username.Location = new System.Drawing.Point(90, 119);
            this.lbl_username.Name = "lbl_username";
            this.lbl_username.Size = new System.Drawing.Size(99, 23);
            this.lbl_username.TabIndex = 2;
            this.lbl_username.Text = "Username";
            // 
            // lbl_email
            // 
            this.lbl_email.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_email.AutoSize = true;
            this.lbl_email.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_email.Location = new System.Drawing.Point(107, 32);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(65, 23);
            this.lbl_email.TabIndex = 0;
            this.lbl_email.Text = "E-mail";
            // 
            // pnl_changePassword2
            // 
            this.pnl_changePassword2.Controls.Add(this.lbl_error_password);
            this.pnl_changePassword2.Controls.Add(this.txtbox_newPassword);
            this.pnl_changePassword2.Controls.Add(this.txtbox_oldPassword);
            this.pnl_changePassword2.Location = new System.Drawing.Point(283, 381);
            this.pnl_changePassword2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnl_changePassword2.Name = "pnl_changePassword2";
            this.pnl_changePassword2.Size = new System.Drawing.Size(415, 139);
            this.pnl_changePassword2.TabIndex = 9;
            // 
            // lbl_error_password
            // 
            this.lbl_error_password.AutoSize = true;
            this.lbl_error_password.Font = new System.Drawing.Font("Arial", 8F);
            this.lbl_error_password.Location = new System.Drawing.Point(3, 103);
            this.lbl_error_password.Name = "lbl_error_password";
            this.lbl_error_password.Size = new System.Drawing.Size(0, 16);
            this.lbl_error_password.TabIndex = 2;
            // 
            // txtbox_newPassword
            // 
            this.txtbox_newPassword.Location = new System.Drawing.Point(60, 63);
            this.txtbox_newPassword.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtbox_newPassword.Name = "txtbox_newPassword";
            this.txtbox_newPassword.Size = new System.Drawing.Size(295, 30);
            this.txtbox_newPassword.TabIndex = 12;
            // 
            // txtbox_oldPassword
            // 
            this.txtbox_oldPassword.Location = new System.Drawing.Point(60, 29);
            this.txtbox_oldPassword.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtbox_oldPassword.Name = "txtbox_oldPassword";
            this.txtbox_oldPassword.Size = new System.Drawing.Size(295, 30);
            this.txtbox_oldPassword.TabIndex = 10;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbl_error_email);
            this.panel1.Controls.Add(this.txtbox_email);
            this.panel1.Location = new System.Drawing.Point(283, 2);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(415, 81);
            this.panel1.TabIndex = 11;
            // 
            // lbl_error_email
            // 
            this.lbl_error_email.AutoSize = true;
            this.lbl_error_email.Font = new System.Drawing.Font("Arial", 8F);
            this.lbl_error_email.Location = new System.Drawing.Point(3, 58);
            this.lbl_error_email.Name = "lbl_error_email";
            this.lbl_error_email.Size = new System.Drawing.Size(0, 16);
            this.lbl_error_email.TabIndex = 6;
            // 
            // txtbox_email
            // 
            this.txtbox_email.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtbox_email.Location = new System.Drawing.Point(60, 23);
            this.txtbox_email.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtbox_email.Name = "txtbox_email";
            this.txtbox_email.Size = new System.Drawing.Size(295, 30);
            this.txtbox_email.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lbl_error_username);
            this.panel2.Controls.Add(this.txtbox_username);
            this.panel2.Location = new System.Drawing.Point(283, 89);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(415, 81);
            this.panel2.TabIndex = 12;
            // 
            // lbl_error_username
            // 
            this.lbl_error_username.AutoSize = true;
            this.lbl_error_username.Font = new System.Drawing.Font("Arial", 8F);
            this.lbl_error_username.Location = new System.Drawing.Point(3, 58);
            this.lbl_error_username.Name = "lbl_error_username";
            this.lbl_error_username.Size = new System.Drawing.Size(0, 16);
            this.lbl_error_username.TabIndex = 7;
            // 
            // txtbox_username
            // 
            this.txtbox_username.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtbox_username.Location = new System.Drawing.Point(59, 26);
            this.txtbox_username.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtbox_username.Name = "txtbox_username";
            this.txtbox_username.Size = new System.Drawing.Size(295, 30);
            this.txtbox_username.TabIndex = 3;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lbl_error_nameSurname);
            this.panel3.Controls.Add(this.txtbox_address);
            this.panel3.Location = new System.Drawing.Point(283, 176);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(415, 81);
            this.panel3.TabIndex = 13;
            // 
            // lbl_error_nameSurname
            // 
            this.lbl_error_nameSurname.AutoSize = true;
            this.lbl_error_nameSurname.Font = new System.Drawing.Font("Arial", 8F);
            this.lbl_error_nameSurname.Location = new System.Drawing.Point(3, 58);
            this.lbl_error_nameSurname.Name = "lbl_error_nameSurname";
            this.lbl_error_nameSurname.Size = new System.Drawing.Size(0, 16);
            this.lbl_error_nameSurname.TabIndex = 8;
            // 
            // txtbox_address
            // 
            this.txtbox_address.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtbox_address.Location = new System.Drawing.Point(60, 22);
            this.txtbox_address.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtbox_address.Name = "txtbox_address";
            this.txtbox_address.Size = new System.Drawing.Size(295, 30);
            this.txtbox_address.TabIndex = 5;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.txtPhoneNumber0);
            this.panel4.Controls.Add(this.lbl_error_address);
            this.panel4.Location = new System.Drawing.Point(283, 263);
            this.panel4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(415, 114);
            this.panel4.TabIndex = 14;
            // 
            // txtPhoneNumber0
            // 
            this.txtPhoneNumber0.Location = new System.Drawing.Point(59, 44);
            this.txtPhoneNumber0.Name = "txtPhoneNumber0";
            this.txtPhoneNumber0.Size = new System.Drawing.Size(295, 30);
            this.txtPhoneNumber0.TabIndex = 7;
            // 
            // lbl_error_address
            // 
            this.lbl_error_address.AutoSize = true;
            this.lbl_error_address.Font = new System.Drawing.Font("Arial", 8F);
            this.lbl_error_address.Location = new System.Drawing.Point(3, 90);
            this.lbl_error_address.Name = "lbl_error_address";
            this.lbl_error_address.Size = new System.Drawing.Size(0, 16);
            this.lbl_error_address.TabIndex = 9;
            // 
            // pnl_changePassword1
            // 
            this.pnl_changePassword1.Controls.Add(this.lbl_newPassword);
            this.pnl_changePassword1.Controls.Add(this.lbl_oldPassword);
            this.pnl_changePassword1.Controls.Add(this.lbl_changePassword);
            this.pnl_changePassword1.Location = new System.Drawing.Point(3, 381);
            this.pnl_changePassword1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnl_changePassword1.Name = "pnl_changePassword1";
            this.pnl_changePassword1.Size = new System.Drawing.Size(274, 139);
            this.pnl_changePassword1.TabIndex = 10;
            // 
            // lbl_newPassword
            // 
            this.lbl_newPassword.AutoSize = true;
            this.lbl_newPassword.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_newPassword.Location = new System.Drawing.Point(52, 70);
            this.lbl_newPassword.Name = "lbl_newPassword";
            this.lbl_newPassword.Size = new System.Drawing.Size(143, 23);
            this.lbl_newPassword.TabIndex = 11;
            this.lbl_newPassword.Text = "New Password";
            // 
            // lbl_oldPassword
            // 
            this.lbl_oldPassword.AutoSize = true;
            this.lbl_oldPassword.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_oldPassword.Location = new System.Drawing.Point(52, 36);
            this.lbl_oldPassword.Name = "lbl_oldPassword";
            this.lbl_oldPassword.Size = new System.Drawing.Size(135, 23);
            this.lbl_oldPassword.TabIndex = 9;
            this.lbl_oldPassword.Text = "Old Password";
            // 
            // lbl_changePassword
            // 
            this.lbl_changePassword.AutoSize = true;
            this.lbl_changePassword.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_changePassword.Location = new System.Drawing.Point(28, 7);
            this.lbl_changePassword.Name = "lbl_changePassword";
            this.lbl_changePassword.Size = new System.Drawing.Size(181, 24);
            this.lbl_changePassword.TabIndex = 8;
            this.lbl_changePassword.Text = "Change Password";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lblPhoneNumber);
            this.panel5.Location = new System.Drawing.Point(3, 264);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(274, 112);
            this.panel5.TabIndex = 15;
            // 
            // lblPhoneNumber
            // 
            this.lblPhoneNumber.AutoSize = true;
            this.lblPhoneNumber.Font = new System.Drawing.Font("Arial", 12F);
            this.lblPhoneNumber.Location = new System.Drawing.Point(56, 46);
            this.lblPhoneNumber.Name = "lblPhoneNumber";
            this.lblPhoneNumber.Size = new System.Drawing.Size(139, 23);
            this.lblPhoneNumber.TabIndex = 6;
            this.lblPhoneNumber.Text = "Phone Number";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(750, 35);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(272, 272);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // btnChangeProPic
            // 
            this.btnChangeProPic.BackColor = System.Drawing.Color.AliceBlue;
            this.btnChangeProPic.ForeColor = System.Drawing.Color.Purple;
            this.btnChangeProPic.Location = new System.Drawing.Point(784, 329);
            this.btnChangeProPic.Name = "btnChangeProPic";
            this.btnChangeProPic.Size = new System.Drawing.Size(214, 33);
            this.btnChangeProPic.TabIndex = 15;
            this.btnChangeProPic.Text = "Change Profile Picture";
            this.btnChangeProPic.UseVisualStyleBackColor = false;
            this.btnChangeProPic.Click += new System.EventHandler(this.btnChangeProPic_Click);
            // 
            // btnRemoveProPic
            // 
            this.btnRemoveProPic.BackColor = System.Drawing.Color.AliceBlue;
            this.btnRemoveProPic.ForeColor = System.Drawing.Color.Purple;
            this.btnRemoveProPic.Location = new System.Drawing.Point(784, 367);
            this.btnRemoveProPic.Name = "btnRemoveProPic";
            this.btnRemoveProPic.Size = new System.Drawing.Size(214, 33);
            this.btnRemoveProPic.TabIndex = 16;
            this.btnRemoveProPic.Text = "Remove Profile Picture";
            this.btnRemoveProPic.UseVisualStyleBackColor = false;
            this.btnRemoveProPic.Click += new System.EventHandler(this.btnRemoveProPic_Click);
            // 
            // btnDeleteAccount
            // 
            this.btnDeleteAccount.BackColor = System.Drawing.Color.AliceBlue;
            this.btnDeleteAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnDeleteAccount.ForeColor = System.Drawing.Color.Purple;
            this.btnDeleteAccount.Location = new System.Drawing.Point(879, 564);
            this.btnDeleteAccount.Name = "btnDeleteAccount";
            this.btnDeleteAccount.Size = new System.Drawing.Size(143, 28);
            this.btnDeleteAccount.TabIndex = 17;
            this.btnDeleteAccount.Text = "Delete Account";
            this.btnDeleteAccount.UseVisualStyleBackColor = false;
            this.btnDeleteAccount.Click += new System.EventHandler(this.btnDeleteAccount_Click);
            // 
            // Profile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Linen;
            this.ClientSize = new System.Drawing.Size(1034, 653);
            this.Controls.Add(this.btnDeleteAccount);
            this.Controls.Add(this.btnRemoveProPic);
            this.Controls.Add(this.btnChangeProPic);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btn_goShop);
            this.Controls.Add(this.tabControl1);
            this.Name = "Profile";
            this.Text = "Profile";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Profile_FormClosed);
            this.Load += new System.EventHandler(this.Profile_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabProfil.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.pnl_changePassword2.ResumeLayout(false);
            this.pnl_changePassword2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.pnl_changePassword1.ResumeLayout(false);
            this.pnl_changePassword1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_goShop;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabProfil;
        private System.Windows.Forms.Button btn_saveChanges;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lbl_username;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.Panel pnl_changePassword2;
        private System.Windows.Forms.Label lbl_error_password;
        private System.Windows.Forms.TextBox txtbox_newPassword;
        private System.Windows.Forms.TextBox txtbox_oldPassword;
        private System.Windows.Forms.Panel pnl_changePassword1;
        private System.Windows.Forms.Label lbl_newPassword;
        private System.Windows.Forms.Label lbl_oldPassword;
        private System.Windows.Forms.Label lbl_changePassword;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_error_email;
        public System.Windows.Forms.TextBox txtbox_email;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lbl_error_username;
        private System.Windows.Forms.TextBox txtbox_username;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lbl_error_nameSurname;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lbl_error_address;
        private System.Windows.Forms.TextBox txtbox_address;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnChangeProPic;
        private System.Windows.Forms.Button btnRemoveProPic;
        private System.Windows.Forms.TextBox txtPhoneNumber0;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblPhoneNumber;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Button btnDeleteAccount;
        private System.Windows.Forms.Panel panel6;
    }
}