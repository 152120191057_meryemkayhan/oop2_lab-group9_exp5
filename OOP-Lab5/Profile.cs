﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

namespace OOP_Lab5
{
    public partial class Profile : Form
    {
        SqlConnection sqlConnection;
        SqlCommand sqlCommand_userDelete;
        SqlCommand sqlCommand_cartDelete;
        SqlCommand sqlCommand_userUpdate;
        SqlDataAdapter sqlDataAdapter;
        Person loggedPerson = new Person();
        public void setPerson(Person storePerson)
        {
            loggedPerson = storePerson;
        }
        public Profile()
        {
            InitializeComponent();
        }

        private void Profile_Load(object sender, EventArgs e)
        {
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox1.Image = Image.FromFile(loggedPerson.PathOfPP);


            txtbox_email.Text = loggedPerson.Email;
            txtbox_username.Text = loggedPerson.Username;
            txtbox_address.Text = loggedPerson.Address;
            txtPhoneNumber0.Text = loggedPerson.PhoneNumber;

            txtbox_oldPassword.PasswordChar = '*';
            txtbox_newPassword.PasswordChar = '*';

            if (loggedPerson.PathOfPP == "pictures\\pp.jpg") btnChangeProPic.Text = "Add a Profile Picture";
            else btnChangeProPic.Text = "Change Profile Picture";

        }
        private void btn_goShop_Click(object sender, EventArgs e)
        {
            this.Close();
            Store store = new Store();
            store.setPerson(loggedPerson);
            store.Show();
        }
        private void btnChangeProPic_Click(object sender, EventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();
            opf.Title = "Select an image";
            opf.Filter = "Image File (*.jpg; *.jpeg; *.png; *.svg;) |*.jpg; *.jpeg; *.png; *.svg;";

            if (opf.ShowDialog() == DialogResult.OK)
            {
                // picturebox içini güncelleme
                Bitmap newPP= new Bitmap(opf.FileName);
                pictureBox1.Image = newPP;

                // yeni seçilen image ın pathini elde etme
                string newDirectory = opf.FileName;
                string fileName = newDirectory.Substring(newDirectory.LastIndexOf(@"\") + 1);
                loggedPerson.PathOfPP = "pictures\\" + fileName;

                // database güncellemesi
                try
                {
                    sqlConnection = new SqlConnection("Data Source=SQL5063.site4now.net; Initial Catalog=db_a75584_ooplab; User ID=db_a75584_ooplab_admin; Password=7eQ2icRhm_au");
                    sqlConnection.Open();
                    string input = "Update Users Set profilePicture=@profilePicture where userID='" + loggedPerson.Id + "'";

                    sqlCommand_userDelete = new SqlCommand(input, sqlConnection);
                    sqlCommand_userDelete.Parameters.AddWithValue("@profilePicture", loggedPerson.PathOfPP);
                    sqlCommand_userDelete.ExecuteNonQuery();

                    sqlConnection.Close();
                    btnChangeProPic.Text = "Change Profile Picture";

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }               
            }
        }
        private void btnRemoveProPic_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = Image.FromFile(@"Pictures\pp.jpg");
            btnChangeProPic.Text = "Add a Profile Picture";
        }
        private void btnDeleteAccount_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Are you sure to delete your account?", 
                Application.ProductName, 
                MessageBoxButtons.YesNo, 
                MessageBoxIcon.Question)==DialogResult.Yes)
            {
                sqlConnection = new SqlConnection("Data Source=SQL5063.site4now.net; Initial Catalog=db_a75584_ooplab; User ID=db_a75584_ooplab_admin; Password=7eQ2icRhm_au");
                sqlCommand_userDelete = new SqlCommand("DELETE FROM Users WHERE userID='" + loggedPerson.Id + "'", sqlConnection);
                sqlCommand_cartDelete = new SqlCommand("DELETE FROM ShoppingCart WHERE UserID='" + loggedPerson.Id + "'", sqlConnection);
                try
                {
                    
                    sqlConnection.Open();
                    sqlCommand_userDelete.ExecuteNonQuery();
                    sqlCommand_cartDelete.ExecuteNonQuery();
                    sqlConnection.Close();
                    this.Close();
                    MessageBox.Show("Your account is deleting.\n You are being redirected to login page.", Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Authentication aut = new Authentication();
                    aut.Show();
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, Application.ProductName, 
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btn_saveChanges_Click(object sender, EventArgs e)
        {
            // EMAİL VE USERNAME DEĞİŞİKLİKLERİN DİĞRE HESAPLARLA KIYASLAMAM GEREKTİĞİ İÇİN TÜM USERLARI ÇEKİYORUM.
            List<Person> People = new List<Person>();

            sqlConnection = new SqlConnection("Data Source=SQL5063.site4now.net; Initial Catalog=db_a75584_ooplab; User ID=db_a75584_ooplab_admin; Password=7eQ2icRhm_au");
            sqlConnection.Open();
            sqlDataAdapter = new SqlDataAdapter("select * from Users", sqlConnection);

            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                Person customer = new Person(int.Parse(dataTable.Rows[i]["userID"].ToString()),
                                                dataTable.Rows[i]["username"].ToString(),
                                                dataTable.Rows[i]["email"].ToString(),
                                                dataTable.Rows[i]["password"].ToString(),
                                                dataTable.Rows[i]["address"].ToString(),
                                                dataTable.Rows[i]["phoneNumber"].ToString(),
                                                dataTable.Rows[i]["profilePicture"].ToString(),
                                                Convert.ToBoolean(dataTable.Rows[i]["isAdmin"]));
                People.Add(customer);
            }
            sqlConnection.Close();
            bool invalidEmail = CompareLib.Compare.detect_emailTaken(People, txtbox_email.Text);
            bool invalidUsername = CompareLib.Compare.detect_usernameTaken(People, txtbox_username.Text);

            // email changes
            if (!String.IsNullOrEmpty(txtbox_email.Text) && invalidEmail==false)
            {
                sqlConnection = new SqlConnection("Data Source=SQL5063.site4now.net; Initial Catalog=db_a75584_ooplab; User ID=db_a75584_ooplab_admin; Password=7eQ2icRhm_au");
                sqlConnection.Open();
                string input = "Update Users Set email=@email where userID='" + loggedPerson.Id + "'";

                sqlCommand_userUpdate = new SqlCommand(input, sqlConnection);
                sqlCommand_userUpdate.Parameters.AddWithValue("@email", txtbox_email.Text);
                sqlCommand_userUpdate.ExecuteNonQuery();
                sqlConnection.Close();

                loggedPerson.Email = txtbox_email.Text;
                lbl_error_email.ForeColor = Color.Green;
                lbl_error_email.Text = "Your email has changed. We sent you an email to confirm.";
            }
            else if(!String.IsNullOrEmpty(txtbox_email.Text) && invalidEmail == true && txtbox_email.Text!=loggedPerson.Email)
            {
                lbl_error_email.ForeColor = Color.Red;
                lbl_error_email.Text = "This e-mail has already related with an another account.";

            }
            else if(String.IsNullOrEmpty(txtbox_email.Text))
            {
                txtbox_email.Text = loggedPerson.Email;
            }
            else // logged user ın kendi emailinin kullanıcı tarafından tekrar girilmesi ya da hiç ellenmemesi durumu
            {
                txtbox_email.Text = loggedPerson.Email;
                lbl_error_email.Text = "";
            }

            // username changes 
            if (!String.IsNullOrEmpty(txtbox_username.Text) && invalidUsername == false)
            {
                sqlConnection = new SqlConnection("Data Source=SQL5063.site4now.net; Initial Catalog=db_a75584_ooplab; User ID=db_a75584_ooplab_admin; Password=7eQ2icRhm_au");
                sqlConnection.Open();
                string input = "Update Users Set username=@username where userID='" + loggedPerson.Id + "'";

                sqlCommand_userUpdate = new SqlCommand(input, sqlConnection);
                sqlCommand_userUpdate.Parameters.AddWithValue("@username", txtbox_username.Text);
                sqlCommand_userUpdate.ExecuteNonQuery();
                sqlConnection.Close();

                loggedPerson.Email = txtbox_email.Text;
                lbl_error_username.ForeColor = Color.Green;
                lbl_error_username.Text = "Your username has changed successfully.";
            }
            else if (!String.IsNullOrEmpty(txtbox_username.Text) && invalidUsername == true && txtbox_username.Text != loggedPerson.Username)
            {
                lbl_error_username.ForeColor = Color.Red;
                lbl_error_username.Text = "This username has already taken.";
            }
            else if (String.IsNullOrEmpty(txtbox_username.Text))
            {
                txtbox_username.Text = loggedPerson.Username;
            }
            else
            {
                txtbox_username.Text = loggedPerson.Username;
                lbl_error_username.Text = "";
            }

            // address changes
            if (!String.IsNullOrEmpty(txtbox_address.Text) && loggedPerson.Address!= txtbox_address.Text)
            {
                sqlConnection = new SqlConnection("Data Source=SQL5063.site4now.net; Initial Catalog=db_a75584_ooplab; User ID=db_a75584_ooplab_admin; Password=7eQ2icRhm_au");
                sqlConnection.Open();
                string input = "Update Users Set address=@address where userID='" + loggedPerson.Id + "'";

                sqlCommand_userUpdate = new SqlCommand(input, sqlConnection);
                sqlCommand_userUpdate.Parameters.AddWithValue("@address", txtbox_address.Text);
                sqlCommand_userUpdate.ExecuteNonQuery();
                sqlConnection.Close();

                loggedPerson.Address = txtbox_address.Text;
                lbl_error_nameSurname.ForeColor = Color.Green;
                lbl_error_nameSurname.Text = "Your address has changed successfully.";
            }
            else if (String.IsNullOrEmpty(txtbox_address.Text))
            {
                txtbox_address.Text = loggedPerson.Address;
            }
            // phone number changes
            if (!String.IsNullOrEmpty(txtPhoneNumber0.Text) && loggedPerson.PhoneNumber != txtPhoneNumber0.Text)
            {
                sqlConnection = new SqlConnection("Data Source=SQL5063.site4now.net; Initial Catalog=db_a75584_ooplab; User ID=db_a75584_ooplab_admin; Password=7eQ2icRhm_au");
                sqlConnection.Open();
                string input = "Update Users Set phoneNumber=@phoneNumber where userID='" + loggedPerson.Id + "'";

                sqlCommand_userUpdate = new SqlCommand(input, sqlConnection);
                sqlCommand_userUpdate.Parameters.AddWithValue("@phoneNumber", txtPhoneNumber0.Text);
                sqlCommand_userUpdate.ExecuteNonQuery();
                sqlConnection.Close();

                loggedPerson.PhoneNumber = txtPhoneNumber0.Text;
                lbl_error_address.ForeColor = Color.Green;
                lbl_error_address.Text = "Your phone number has changed successfully.";
            }
            else if (String.IsNullOrEmpty(txtPhoneNumber0.Text))
            {
                txtPhoneNumber0.Text = loggedPerson.PhoneNumber;
            }


            //password check
            if (!String.IsNullOrEmpty(txtbox_oldPassword.Text) && String.IsNullOrEmpty(txtbox_newPassword.Text))
            {
                lbl_error_password.ForeColor = Color.Red;
                lbl_error_password.Text = "You must enter new password";
            }
            else if (String.IsNullOrEmpty(txtbox_oldPassword.Text) && !String.IsNullOrEmpty(txtbox_newPassword.Text))
            {
                lbl_error_password.ForeColor = Color.Red;
                lbl_error_password.Text = "You must enter current password";
            }            
            else if (!String.IsNullOrEmpty(txtbox_newPassword.Text) && !String.IsNullOrEmpty(txtbox_oldPassword.Text))
            {
                if (CryptoLib.Encryptor.MD5Hash(txtbox_oldPassword.Text) != loggedPerson.Password)
                {
                    lbl_error_password.ForeColor = Color.Red;
                    lbl_error_password.Text = "You entered wrong your current password.";
                }
                else
                {
                    // database şifre güncelleme işlemi burada yapılır
                    sqlConnection = new SqlConnection("Data Source=SQL5063.site4now.net; Initial Catalog=db_a75584_ooplab; User ID=db_a75584_ooplab_admin; Password=7eQ2icRhm_au");
                    sqlConnection.Open();
                    string input = "Update Users Set password=@password where userID='" + loggedPerson.Id + "'";
                    sqlCommand_userUpdate = new SqlCommand(input, sqlConnection);
                    sqlCommand_userUpdate.Parameters.AddWithValue("@password", CryptoLib.Encryptor.MD5Hash(txtbox_newPassword.Text));
                    sqlCommand_userUpdate.ExecuteNonQuery();

                    sqlConnection.Close();
                    lbl_error_password.ForeColor = Color.Green;
                    lbl_error_password.Text = "Your password has changed successfully.";
                }
            }

        }

        private void Profile_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                Store store = new Store();
                store.setPerson(loggedPerson);
                store.Show();
            }
        }
    }
}
